package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // küsime kasutajalt pin koodi, kui see on õige, siis ütleme: tore
        //muul juhul küsime uuesti
        //kokku küsime 3 korda

        String realPin = "1234";

        Scanner scanner = new Scanner(System.in);
        String enteredPin = "";

//        for (int i = 0; i < 3; i++) {
//        System.out.println("Sisestage PIN");
//
//
//            enteredPin=scanner.nextLine();
//            if (enteredPin.equals(realPin)) {
//                System.out.println("Tore! Õige pin!");
//                break;
//            } else{
//                System.out.println("Vale pin! Proovi uuesti");
//                if(i==2){
//                    System.out.println("Proovisite liiga palju. Konto on suletud");
//                }
//            }
//
//        }

        //sama asi do ja whileiga


        int retriesLeft = 3;
        do {
            System.out.println("Sisestage PIN");
            retriesLeft--;
            enteredPin = scanner.nextLine();

            if (enteredPin.equals(realPin)) {
                System.out.println("Tore! Õige pin!");
                break;
            }
        }
            while (!enteredPin.equals(realPin) && retriesLeft > 0); {
                System.out.println("Liiga palju proovitud!");

            }


            //prindi välja 10 kuni 20 ja 40-60
        //continue jätab tsüklikorduse katki ja läheb järgmise korduse juurde
        for (int i = 10; i <= 60 ; i++) {
            if (i>20 && i < 40){
                continue;
            }
            System.out.println(i);
        }



        }
    }

