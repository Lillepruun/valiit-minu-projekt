package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {GAS, DIESEL, PETROL, HYBRID, ELECTRIC}
public class Car {

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;
    private Person driver;
    private Person owner;

    private int maxPassangers;
    private int currentPassangers;
    private Person passanger;
    private List<Person> passangers = new ArrayList<Person>();


    public List<Person> getPassangers() {
        return passangers;
    }



    public void addPassangerObjToPassangerArraylist(Person person){

        if (passangers.contains(person)){
            System.out.println("Isik on juba reisijate hulgas. Ei saa reisijat lisada. ");
            return;
        }
        if (passangers.size()<maxPassangers){
            passangers.add(person);
            System.out.println(person.getFirstName() + " istus autosse ");
        } else {
            System.out.println("Auto on täis. Ei saa reisijaid lisada");
        }
    }
    public void removePassangerObjFromPassangerArraylist(Person person){
        passangers.remove(person);
        System.out.println(person.getFirstName() + " eemaldati reisijate hulgast");
    }

    public void showPassengers(){

        // foreach loop
        //iga elemendi kohta listis tekita objekt passenger
        //1. kordus Person passenger on esimene reisija
        //2. kordus Person passanger on teine reisija
        System.out.println("Autos on järgnevad reisijad");
        for (Person  passanger :passangers) {
            System.out.println(passanger.getFirstName());
        }
    }



    public void setPassangersArray(List<Person> passangers) {
        if (passangers.size()<=maxPassangers){
            this.passangers = passangers;
            currentPassangers = passangers.size();
            System.out.println("Autosse istus "+passangers.size()+" reisijat");
        }else {
            System.out.println("autosse ei mahu nii palju reisijaid");
        }
    }

    public void printNamesOfPassangers(){
        System.out.println();
        System.out.println("Autos reisijate nimed");
        for (int i = 0; i <passangers.size() ; i++) {
            System.out.println(passangers.get(i).getFullNameOfPerson());
        }
        System.out.println();
    }



    public int getMaxPassangers() {
        return maxPassangers;
    }

    public void setMaxPassangers(int maxPassangers) {
        if (maxPassangers>= 0){
            this.maxPassangers = maxPassangers;
        }else {
            System.out.println("Autosse ei saa mahtuda negatiivne arv reisijaid");
        }

    }

    public int getCurrentNrOfPassangers() {
        return getPassangers().size();
    }
            //ei ole vaja kuna saame currentpassangeri passangerArray.size.iga
//    public void setCurrentNrOfPassangers(int currentPassangers) {
//        if (currentPassangers<=maxPassangers){
//            this.currentPassangers = currentPassangers;
//        } else {
//            System.out.println("Autosse ei mahu nii palju reisijaid");
//        }
//
//    }




    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }






    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driverName) {
        this.driver = driverName;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }
//Konstruktor constructor
    //on eriline meetod, mis käivitatakse klassist objekti loomisel

    public Car(){
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed=true;
        fuel = Fuel.PETROL;
    }
    //Conssttructor overloading
    public Car(String make, String model, int year, int maxSpeed){
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }
    public Car(String make, String model, int year, int maxSpeed,Fuel fuel,boolean isEngineRunning,boolean isUsed){
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
        this.fuel=fuel;
        this.isEngineRunning = isEngineRunning;
        this.isUsed = isUsed;
    }



    public void startEngine(){
        if (!isEngineRunning){
            isEngineRunning=true;
            System.out.println("Mootor käivitus");
        }else {
            System.out.println("Mootor juba töötab!");
        }
    }
    public void stopEngine(){
        if (isEngineRunning){
            isEngineRunning=false;
            System.out.println("Mootor seisati");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }
    public void accelerate(int targetSpeed){
        if (!isEngineRunning){
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        }else if (targetSpeed>maxSpeed){
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d %n",maxSpeed);
        }else if (targetSpeed<0){
            System.out.printf("Auto kiirus ei saa olla negatiivne");
        }else if (targetSpeed< speed){
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        }else {
            speed=targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d %n",speed);
        }

    }
    public  void deceleration(int targetSpeed){
        if (!isEngineRunning){
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        }  else if (targetSpeed <0){
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed>speed){
            System.out.println("Auto ei saa aeglustada kiiremale kiirusele");
        }else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas kiiruseni %d %n",speed);
        }
    }
    public void park(){
        deceleration(0);
        stopEngine();
        System.out.println("Auto pargiti");
    }
    void printInfo (){
        System.out.println();
        System.out.println("Auto info:");
        System.out.printf("Tootja: %s %n",make);
        System.out.printf("Mudel: %s %n",model);
        System.out.printf("Aasta: %s %n",year);
        System.out.printf("Fuel: %s %n",fuel);
        System.out.printf("Kasutatud: %s %n",isUsed);
        System.out.printf("Kas mootor töötab: %s %n",isEngineRunning);
        System.out.printf("Hetkeline kiirus: %d %n",speed);
        System.out.printf("Max kiirus: %d %n",maxSpeed);
        System.out.printf("Autojuht: %s %n",driver.getFullNameOfPerson());
        System.out.printf("Omanik: %s %n",owner.getFullNameOfPerson());
        System.out.println();
    }


        //KODUS!!!
    //slowdown(int targetSpeed)

    //park() mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid)
    //Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid

    //Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik
    //Prindi välja auto omaniku vanus, (läbi auto obj)

    //Lisa autole max reisijate arv
    //Lisa autole võimalus hoida reisijaid
    //Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    //Kontrolli ka, et ei lisaks rohkem reisijaid kui mahub

}
