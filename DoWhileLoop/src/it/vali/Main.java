package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        System.out.println("Kas tahad jätkata? Jah/Ei");
//        // Jätkame seni kuni kasutaja kirjutab ei
//
//        Scanner scanner = new Scanner(System.in);
//        String input = scanner.nextLine();
//
//
//        while (!(input.toLowerCase().equals("ei".toLowerCase()))) {
//            System.out.println("mäng käib");
//            input = scanner.nextLine();
//
//
//        }

        // do while tsükkel on nagu while tsükkel ainult, et kontroll tehakse pärast tsüklit
        //1 kord tehakse alati, siis kontrollitakse kas teha veel
        Scanner scanner = new Scanner(System.in);
        String input;

        //iga muutuja mille me deklareerime kehtib ainult seda ümbritsevate {} sees
        do {
            System.out.println("Kas tahad jätkata? Jah/Ei");
            input = scanner.nextLine();
        }
        while (!input.equals("ei")); {
            // seda loogsulgu pole tegelikult vaja, aga võib
            System.out.println("Lõpetasite mängu.");
        }

    }
}
