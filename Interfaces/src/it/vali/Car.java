package it.vali;

public class Car extends Vehicle implements Driver {

    private int maxDistance = 600;

    private String make;

    public Car() {

    }


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }





    public Car(int maxDistance){
        this.maxDistance = maxDistance;
    }

    @Override
   public int getMaxDistance(){
       return maxDistance;
   }
   @Override
    public void drive(){
        System.out.println("Sõidan autoga");
    }
    @Override
    public void stopDriving (int afterDistance){
        System.out.println("Peatan auto " + afterDistance +" km pärast");
    }


    @Override
    public String toString() {
        return " Auto andmed: " + make;
    }
}
