package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
	// write your code here

    int[] numbers = new int[5];
        for (int i = 0; i <numbers.length ; i++) {
            System.out.println(numbers[i]);
        }

        //lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;
        numbers[2] = 0;
        numbers[3] = -4;
        numbers[4] = 0;

        //eemalda siit  teine number
        numbers[1]=0;

        //vaheta 2 ja -1




//        ArrayList arrayList = new ArrayList();
//        for (int i = 0; i < 10; i++) {
//            arrayList.add(i, i);
//        }
//        for (int i = 0; i <arrayList.size() ; i++) {
//            System.out.println(arrayList.listIterator());
//        }

        //Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemente
        //aga elemente küsides pean ma teadma, mis tüüpi element kus täpselt asub
        //ning pean ka selleks tüübiks küsimisel ka cast-ima


        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);


        double money = 24.55;
        Random random = new Random();
        list.add(money);
        list.add(random);

        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));
        }
        //pidin intiks teisendama
        int a = (int) list.get(1);
        System.out.println(a);
        String word = (String) list.get(0);

        int sum = 3 +(int) list.get(1);

        String sentence = list.get(0) + " hommikust";

        System.out.println(sentence);

        if (Integer.class.isInstance(a)){
            System.out.println("a on int");
        }
        if (String.class.isInstance(list.get(0))){
            System.out.println("listis indeksiga 0 on String");
        }


        System.out.println();
        //lisab elemendi, ei kustuta midagi, paneb vahele
        list.add(2,"someString");
        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));
        }
        //lisab terve nimekirja esimesse nimekirja

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(3);
        otherList.add(true);
        list.addAll(otherList);
        System.out.println();

        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));
        }
        if (list.contains(money)){
            System.out.println("money asub listis");
        }
        System.out.printf("money asub listis indeksiga %d %n",list.indexOf(money));

        System.out.println(list.get(2).getClass());
        list.remove(money);
        System.out.println();

        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));
        }

        //kirjutab üle
        list.set(0,"Tore");














    }
}
