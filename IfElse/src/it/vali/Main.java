package it.vali;

public class Main {

    public static void main(String[] args) {
        int a = 23;
// Kui arv on 4, siis prindi arv on 4
        //muuljuhul, kui arv on negatiivne, siis kontrolli kas arv on suurem, kui -10, prindi tekst
        //muuljuhul kontrolli kas arv on suurem kui 20, prindi tekst
        if (a == 4) {
            System.out.println("Arv on 4");
        } else {
            if (a < 0) {
                System.out.println("Arv on negatiivne");

                if (a > -10) {
                    System.out.println("Arv on suurem kui -10");
                }
            } else {
                if (a > 20) {
                    System.out.println("Arv on suurem kui 20");
                }
            }

        }
    }
}
