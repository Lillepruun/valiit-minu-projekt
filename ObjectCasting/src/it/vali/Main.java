package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here

        int a = 100;
        short b = (short) a;

        //iga kassi võib võtta kui looma
        //implicit casting
        //Animal animal = new Cat();
        Animal animal = new Animal();

        //iga loom ei ole kass

        //Cat cat = (Cat) new Animal();

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        Cow cow = new Cow();
        cow.setAge(3);
        cow.setBreed("mingitõug");
        animals.add(dog);
        animals.add(cow);
        animals.add(new Pig());
        animals.add(new Pet());
        ((Pet)animals.get(animals.size()-1)).setLifeExpectancy(7);
        animals.add(new HerbiVoreAnimal());



        animals.get(animals.indexOf(dog)).setName("Peeter");

        //kutsu kõikide listis olevate loomade printInfo välja
//        for (int i = 0; i <animals.size() ; i++) {
//            (Animal)animals[i].
//        }
        Animal secondAnimal = new Dog();

        //teisendan tagasi dogiks, siis saan ka dogi meeetoditele ligi
        ((Dog)secondAnimal).setHasTail(true);
        secondAnimal.printInfo();


        for (Animal animalInList: animals
             ) {
            animalInList.printInfo();

        }







    }
}
