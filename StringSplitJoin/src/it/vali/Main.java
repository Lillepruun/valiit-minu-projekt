package it.vali;


public class Main {

    public static void main(String[] args) {
	// write your code here

        String [] words = new String[] {"Põdrala","maja","metsa","sees"};

        System.out.println("massiiv sisaldab: \n");

        for (int i = 0; i < words.length; i++) {
            System.out.print(words [i] + " ");
        }


        System.out.println();
        System.out.println();
        //prindi kõik sõnad massiivist mis algavad m - tähega
        System.out.println("prindi kõik sõnad massiivist mis algavad m - tähega ");
        for (int i = 0; i < words.length; i++) {

            //String firstLetter = words [i].substring(0,1);
            if (words[i].substring(0,1).equals("m")) {
                System.out.println(words[i]);
            }
        }

        //prindi kõik sõnad massiivist mis algavad m - tähega  KASUTADES CharAt
        System.out.println("prindi kõik sõnad massiivist mis algavad m - tähega - KASUTADES CharAt ");
        for (int i = 0; i < words.length; i++) {

            //String firstLetter = words [i].charAt(0);
            //String Emm = ("m");
            if (words [i].charAt(0) =='m') {
                System.out.println(words[i]);
            }
        }





        System.out.println();
//sama asi lisa muutujaga
        System.out.println("prindi kõik sõnad massiivist mis algavad m - tähega  KASUTADES LISAMUUTUJAT firstLetter");
        for (int i = 0; i < words.length; i++) {

            String firstLetter = words [i].substring(0,1);
            if (firstLetter.toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        //prindi kõik a tähega lõppevad sõnad!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        System.out.println("prindi kõik a tähega lõppevad sõnad! \n");


        for (int i = 0; i < words.length; i++) {


            if (words[i].endsWith("a")) {
            //if ("a".words [i].charAt(words[i].length()-1)).e {
                System.out.println(words[i] + " -on a tähega lõppev sõna : kasutades endswith");
            }
        }

        for (int i = 0; i < words.length; i++) {

            String lastLetter = words [i].substring(words[i].length()-1);
            if (lastLetter.toLowerCase().equals("a")) {
                //if ("a".words [i].charAt(words[i].length()-1)).e {
                System.out.println(words[i] + " -on a tähega lõppev sõna : kasutades lisamuutujat lastLetter");
            }
        }


        System.out.println();
        System.out.println();

        //Loe üle kõik sõnad mis sisaldavad a tähte


        System.out.println("Loe üle kõik sõnad mis sisaldavad a tähte \n");
        int counter = 0;

        for (int i = 0; i < words.length; i++) {

            if (words [i].contains("a")) {
                System.out.println("sõna " + words[i] + " sisaldab tähte a: " + words [i].contains("a"));
                counter++;

            }
            else { System.out.println("sõna " + words[i] + "!!! EI sisalda tähte a: " + words [i].contains("a"));

            }

        }
        System.out.println("a tähega sõnasid on: " + counter);


        System.out.println();

        //Prindi välja kõik sõnad kus on 4 tähte
        System.out.println("prindi välja kõik sõnad kus on 4 tähte\n");

        for (int i = 0; i < words.length; i++) {

            if (words[i].length() == 4) {
                System.out.println(words[i ]+ " -sisaldab 4 tähte");
            }
        }

        System.out.println();

        //Prindi välja kõige pikem sõna
        System.out.println("Prindi välja kõige pikem sõna");
        int longestWordLength  = -1;
        int longestWordIndex =-1;
        for (int i = 0; i < words.length; i++) {

            if (words [i].length() > longestWordLength) {
                longestWordLength = words [i].length();
                longestWordIndex = i;

            }

        }
        System.out.println(words[longestWordIndex]+ " - on pikim sõna");

        System.out.println();



        //Prindi välja sõna kus on esimene ja viimane täht sama
        System.out.println("Prindi välja sõna kus on esimene ja viimane täht sama");
        //System.out.println(words[i ]+ " " + words[i].substring(0,1)); substringiga saaks vist ka aga järgnev on lihtsam

        for (int i = 0; i < words.length; i++) {

            if (words[i].charAt(0)==(words[i].charAt(words[i].length()-1))) {

           // if (words[i].substring(0,1).equals(words[i].charAt(0),words[i].length())) {
                System.out.println(words[i ]+ " -on sõna kus  esimene ja viimane täht sama : CharAt");
            }

        }

        for (int i = 0; i < words.length; i++) {

            if (words[i].substring(0,1).equals(words[i].substring(words[i].length()-1))) {

                // if (words[i].substring(0,1).equals(words[i].charAt(0),words[i].length())) {
                System.out.println(words[i ]+ " -on sõna kus  esimene ja viimane täht sama : subString");
            }

        }



    }
}
