package it.vali;

public class Main {


    static int a = 3;

    public static void main(String[] args) {
	// write your code here
        int b = 7;
        System.out.println(b + a);
        System.out.println();

        a = 0;
        System.out.println(increaseByA(12));
        System.out.println();

        //syhadowing, ehk siis seespool defineeritud muutjua varjutab väljaspool oleva muutuja
        int a =6;
        System.out.println(b + a);

        //kui Main.a ei teeks, siis võtaks praegu a=0
        //Nii saan klassi muutuja väärtust muute
        Main.a = 8;
        System.out.println(increaseByA(10));

        System.out.println(b + Main.a);


    }


    static int increaseByA(int b){
        return b + a;
    }
}
