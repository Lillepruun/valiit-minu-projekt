package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        String listFirstName = "Jaan";

        String listLastName = "Lillepruun";


        //Kõigepealt küsitakse külastaja nime
        // kui nimi on listis, siis öeldakse kasutajale
        // Teretulemast Lauri!
        //ja küsitakse külastaja vanust
        //kui kasutaja on alaealine, siis teavitatakse teda, et sorry, sisse ei pääse
        //muul juhul öeldakse, Tere tulemast klubisse

        //Kui kasutaja ei olnud listis
        //küsitakse kasutajalt ka tema perekonnanimi
        //Kui Perenimi on on listis, siis öeldakse Tere tulemast Lauri Mattus
        //Muul juhul öeldakse, "Ma ei tunne sind"
        //
        System.out.println("Mis teie eesnimi on?");
        Scanner scanner = new Scanner(System.in);

        String sisestatudNimi = scanner.nextLine();

        //et erinevaid kirjapilte võrrelda, teeme üheks tüübiks .toLowerCase() või upper

        if (sisestatudNimi.toLowerCase().equals(listFirstName.toLowerCase()) ) {
            System.out.printf("Tere tulemast %s! %n",listFirstName.toUpperCase());
            System.out.println("Kui vana te olete?");



            int sisestatudVanus = Integer.parseInt(scanner.nextLine());

            if (sisestatudVanus>=18) {
                System.out.println("Tere tulemast klubisse!");

            }else if(sisestatudVanus<18){
                System.out.println("Sorry Mate! Natsa noor oled veel.");
            }
        } else {
            System.out.println("Mis on teie perekonnanimi?");
            String sisestatudPerenimi = scanner.nextLine();
            if (sisestatudPerenimi.toLowerCase().equals(listLastName.toLowerCase())){
                System.out.printf("Tere tulemast %s %s %n Meeldivat pidu!",listFirstName,listLastName);
            } else {
                System.out.println("Ma ei tunne sind. Palun lahkuge!");
            }

        }
    }
}
