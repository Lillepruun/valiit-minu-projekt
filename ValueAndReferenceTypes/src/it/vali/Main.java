package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// Kui panna üks primitiivne(value type) tüüpi muutuja võrduma teise muutujaga
        // siiis tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna

        //sama juhtub ka primitive value type tüüpi muutuja kaasa andmisel meetodi parameetriks


        int a = 3;
        int b = a;
        a = 7;

        increment(b);
        System.out.println(b);

        //refernce type (viit- tüüpi) muutuja omistamisel
        //kui panna muuutuja cõrduma teise muutujaga, siis tegelikult jääb mälus ikkagi alles ainult 1 muutuja
        //lihtsalt teine muutuja hakkab viitama samale kohale mälus (samale muutujale)
        //meil on kaks muutujat aga tegelikult nad on täpselt sama objekt


        int [] numbers = new int[]{-5,2,3};
        int [] secondNumbers = numbers;
        numbers[0] = 3;
        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;

        pointB.x=7;
        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;
        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);


        pointA.printNotStatic();
        Point.printStatic();

        increment(numbers);

        Point pointC =new Point();
        pointC.x=12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);


        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();


    }






    public static void increment (int a) {
        a++;
        System.out.printf("a on nüüd %d %n",a);
    }
    
    //suurendame x ja y koordinaate 1 võrra
    public static void increment (Point point){
        
        point.x++;
        point.y++;
        
    }


    //suurendame kõiki elemente 1 võrra
    public static void increment(int[] numbers){
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
            System.out.println(numbers[i]);
        }
    }

}
