package it.vali;
//public tähendab, et klass, meetod või muutuja on avalikult
//nähtav/ligipääsetav
//class - javas üksus, üliselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
/*afhzhfdzhfhdfdfhdzfhdzdfzhdfhzdz*/

//static - meetodi ees tähendab, et seda meetodit saab välja kutsuda ilma klassist
//objekti loomata
//HelloWorld - klassi nimi, mis on ka faili nimi
//void - meetod ei tagasta midagi
// meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse eraldades komaga.
// String tähistab stringi massiivi.
//args - massiivi nimi, sisaldab käsurealt kaasa pandud parameetreid.
//System.out.println - java meetod millega saab printida rida teksti. See mis kirjutatakse sulgudesse
//kirjutatakse välja ja tehakse reavahetus

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("whats up?");
        int arv=1+2;
        System.out.println(arv);


    }
}
