package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";

        String [] words = sentence.split(" ");
        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);

        }

        //Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi

        System.out.println();
        String [] words1 = sentence.split(" ja | |, ");
        for (int i = 0; i < words1.length ; i++) {
            System.out.println(words1[i]);

        }

        System.out.println();

        String newSentence = String.join(" ",words);
        System.out.println(newSentence);
        newSentence = String.join(", ",words);
        System.out.println(newSentence);
        newSentence = String.join(" ja ",words);
        System.out.println(newSentence);

        System.out.println();

        newSentence = String.join("\t",words);
        System.out.println(newSentence);

        //\n jne on escape characters

//        System.out.println("Juku ütles:\\n \"Mulle meeldib suvi\"");
//
//        System.out.println("C:\\Users\\Public");
//        System.out.println();
//
//        System.out.println("Juku\b\b\b ütles:\\n \"Mulle meeldib suvi\"");
//        System.out.println();
//        System.out.println("Juku\007 ütles:\\n \"Mulle meeldib suvi\"");

        //Kõsi kasutajalt rida numbreid nii, et tta paneb need numbrid kirja ühele reale
        //eraldades tühikuga. Seejärel liidab need kõik numbrid kokku ja prindib vastuse.

        System.out.println("Sisesta mnumbrid mida tahad liita, eraldades tühikuga");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
        String numbersText = scanner.nextLine();

            String[] numbers = numbersText.split(" ");

            int sum = 0;
            System.out.print("SISESTASID: ");

            for (int i = 0; i < numbers.length; i++) {

                int numbersInt = Integer.parseInt(numbers[i]);
                sum = sum + Integer.parseInt(numbers[i]);
                System.out.print(numbers[i] + ", ");

            }
            System.out.println();
            System.out.println("SUMMA ON: " + sum);

            String joinedNumbers = String.join(", ", numbers);
            System.out.printf("Arvude %s summa on %d%n", joinedNumbers, sum);
        }

//        while(scanner.hasNextLine()){
//            int sth = scanner.nextInt();
//            System.out.print(sth + scanner.nextInt()+ "\n");
//        }





    }
}
