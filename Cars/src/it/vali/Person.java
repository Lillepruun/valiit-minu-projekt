package it.vali;


enum Gender{
    FEMALE, MALE, NOTSPECIFIED
}


public class Person {
   private String firstName;
   private String lastName;

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    private  Gender gender;
   private int age;


   //konstruktori tunneb ära selle järgi, et tal ei ole return type (void, int jne)
    //Kui klassil ei ole defineeritud konstruktorit, siis tegelikult tehakse nähtamatu
    //parameetrite konstruktor, mille sisu on tühi

    //Kui klassisle ise lisada konstruktor, siis see nähtamatu parameetrita konstruktor kustutatakse
    //sellest klassist saab siis teha objekti ainult selle uue konstruktoriga
    //Kui siiski tahad tühja konstruktorit, siis peab ka selle ise looma uuesti
    //public Person(){}

   public Person(String firstName,String lastName ,Gender gender,int age){
       this.firstName = firstName;
       this.lastName= lastName;
       this.gender=gender;
       this.age = age;
   }

   public String getFirstName(){
       return firstName;
   }

    public String getFullNameOfPerson(){
        return firstName +" " + lastName;
    }

    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonnanimi on: %s, vanus: %d", firstName,lastName,age);
    }
}
