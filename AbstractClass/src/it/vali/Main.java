package it.vali;

public class Main {

    public static void main(String[] args) {
        // Abstraktne klass on hübriid liidesest ja klassist.
        //selles klassis saab defineerida nii meetodi struktuure nagu interfaces
        //aga saab ka defineerida meetodi koos sisuga
        //Abstraktsest klassist ei saa otse objekti luua, saab ainult pärineda


        Kitchen kitchen = new ApartmentKitchen();

        kitchen.becomeDirty();
        ApartmentKitchen apartmentKitchen=new ApartmentKitchen();
        apartmentKitchen.becomeDirty();



    }
}