package it.vali;

public class Bird implements Flyer{

//Bird klass kasutab/implementeerib liidest Flyer
    @Override
    public void fly() {
        jumpUp();
        System.out.println("Fly bird");
    }


    private void jumpUp(){
        System.out.println("Lind hüppas õhku");
    }
}
