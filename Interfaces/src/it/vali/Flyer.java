package it.vali;


//Interface ehk liides sunnib seda kasutavad/implementeerivat klassi omama liideses kirja pandud meetodeid
//(sama tagastuse tüübiga) ja sama parameetrite kombinatsiooniga

//iga klass mis interface kasutab, määrab ise ära meetodi sisu


public interface Flyer {

      void fly();


}
