package it.vali;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here

        //Code menüüst saan vaadata meetodeid, mida saan overridida

        //1 arvuta ringi pindala kui teada on raadius
        //prindi pindala välja ekraanile

        //2 Kirjuta meetod mis kirjutab boolean tüüpi väärtuse ja mille sisendparameetriteks on kaks Stringi
        //Meetod tagastab kas tõene või vale selle kohta, kas stringid on võrdsed

        //3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi
        // Iga sisend massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
        //3,6,7
        //aaa", aaaaaa, jne

        //4. kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja
        //tagastab kõik sellel sajandil esinenud liigaastad
        //sisestada saab ainult vahemikus 500-2019
        //Ütle veateade, kui aastaarv ei mahu vahemikku

        //5.Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list  riikide nimedega
        //kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        //riikide nimekirja eraldades komaga
        //Tekita antud klassis üks objekt ühe vabalt valitud keele andmetega ning prindi välja selle objekti toString() meetodi sisu.

        circleArea(40);

        System.out.println(ifStringsEqual("tere", "tere"));
        System.out.println(ifStringsEqual("tere", "hei hei"));

        int[] myIntArray = new int[]{2, 5, 1, 3};

        System.out.println();

        String[] someStringArray = intArrayTostringArray(myIntArray);


        for (int i = 0; i < someStringArray.length; i++) {
            System.out.println(someStringArray[i]);
        }

        List<Integer> years = leapYearsInCentury(1897);

        for (int year:years) {
            System.out.println(year);
        }
        //System.out.println(returnLongYears("1919"));

        Language language = new Language();
        language.setLanguageName("English");

        List<String> englishCountryNames = new ArrayList<String>();
        englishCountryNames.add("England");
        englishCountryNames.add("USA");
        englishCountryNames.add("Canada");
        englishCountryNames.add("Australia");
        englishCountryNames.add("India");
        language.getCountryNames(englishCountryNames);

        System.out.println(language.toString());







    }

    public static void circleArea(double radius) {
        double area = Math.PI * Math.pow(radius, 2);
        System.out.println("Ringi Pindala on: " + area);
    }

    //2 Kirjuta meetod mis kirjutab boolean tüüpi väärtuse ja mille sisendparameetriteks on kaks Stringi
    //Meetod tagastab kas tõene või vale selle kohta, kas stringid on võrdsed
    public static boolean ifStringsEqual(String string, String otherstring) {
        if (string.equals(otherstring)) {
            return true;
        }
        return false;
    }

    //3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi
    // Iga sisend massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
    //3,6,7
    //aaa", aaaaaa, jne

    public static String[] intArrayTostringArray(int[] intArray) {
        String[] words = new String[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            words[i] = generateAString(intArray[i]);

        }
        return words;
    }


    static String generateAString(int count){
        String word = "";
        for (int i = 0; i <count ; i++) {
            word += "a";
        }
        return word;
    }



    //4. kirjuta meetod, mis võtab sisendparameetrina aastaarvu ja
    //tagastab kõik sellel sajandil esinenud liigaastad
    //sisestada saab ainult vahemikus 500-2019
    //Ütle veateade, kui aastaarv ei mahu vahemikku

    static List<Integer> leapYearsInCentury(int year){
        if (year<500|| year> 2019){
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return new ArrayList<Integer>();
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart +99;
        int leapYear = 2020;
        List<Integer> years = new ArrayList<Integer>();
        for (int i = 2020; i >= centuryStart; i-=4) {
            if (i < centuryEnd && i%4 ==0){
                years.add(i);
            }
        }
        return years;
    }

//    public static int returnLongYears(String year) {
//        int intYear = Integer.parseInt(year);
//        if (intYear < 500 || intYear > 2019) {
//            System.out.println("aastaarv ei mahu 500-2019 vahele");
//            return 0;
//        }
//        Date date = null;
//        try {
//            date = new SimpleDateFormat("yyyy").parse(year);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//
//        int century = (calendar.get(Calendar.YEAR) / 100) + 1;
        //Date dateCentury = new SimpleDateFormat("yyyy").parse(century);

//        if (((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0))
//
//        return  century;
//




}
