package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

//        try {
//            FileWriter fileWriter = new FileWriter("output.txt",true);
//            //for (int i = 0; i < 10; i++) {
//                fileWriter.append("Tere\r\n");
//                fileWriter.close();
//            //}
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        // 1.Koosta täisarvude massiiv kümnest arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2
        String somesentence = "23 -45 67 79 -32 3 5 1 4 -6";

        int [] someIntArray = {23,-45, 67, 79, -32, 3, 5, 1, 4, -6};
//        for (int i = 0; i < someIntArray.length; i++) {
//            //System.out.println(someStringArray[i]);
//        }

        try {
            FileWriter fileWriter = new FileWriter("output.txt",true);
            fileWriter.write("Arvud arrayst, mis on suuremad kui 2: \r\n");

            for (int i = 0; i < someIntArray.length; i++) {
                if (someIntArray[i]>2) {
                    fileWriter.write(someIntArray[i] + ", \r\n");
                }

        }

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println();
        System.out.println();

        // 2.Küsi kasutajalt kaks arvu, küsi seni kuni mõlemad on korrektsed (on numbriks teisendatavad), ning nende summa ei ole paarisarv.

        int sum;
        boolean wrongInput;
        Scanner scanner = new Scanner(System.in);
        do {


        System.out.println("Sisesta kaks arvu mille summa ei ole paarisarv");
        System.out.println("Sisesta esimene arv ning vajuta enter");

            int firstUserInput = 0;
            do {
            wrongInput = false;
            try {
                firstUserInput = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                wrongInput = true;
                System.out.println("ERROR: Sisend ei ole number! Sisesta uus number");
            }
            } while (wrongInput);

            System.out.println("Sisesta teine arv ning vajuta enter");
            int secondUserInput = 0;
            do {
                wrongInput = false;
                try {
                    secondUserInput = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    wrongInput = true;
                    System.out.println("ERROR: Sisend ei ole number! Sisesta uus number");
                }
            } while (wrongInput);



            System.out.println();
            System.out.println();


            sum = firstUserInput + secondUserInput;
            if (sum %2 == 0){
                System.out.println("Arvude summa on paarisarv. Proovi uuesti");
            }
        } while (sum %2 == 0);

        System.out.println("Sisestasid õiged numbrid. Hea töö!");
        // 3. Küsi kasutjalt mitu arvu ta tahab sisestada, seejärel küsi ühe kaupa kasutajalt neeed arvud
        //ning klirjuta nende arvude summa faili, nii et see lisatakse alati juurde. (Append)

        System.out.println("Sisesta numbrid, mille summa lisatakse faili");
        System.out.println("Mitu numbrit soovite sisesatada?");

        int howManyNumbersEntered = Integer.parseInt(scanner.nextLine());
        int appendSum = 0;
        boolean secondwronginput;
        do {
        secondwronginput=false;

        try {


            for (int i = 0; i < howManyNumbersEntered; i++) {
                System.out.println("Sisesta " + (i+1) + ". arv");
                 int input = Integer.parseInt(scanner.nextLine());
                appendSum += input;
                }
        } catch (NumberFormatException e){
            secondwronginput= true;
            appendSum = 0;
            System.out.println("Error: Vale sisend. Sisesta number.");
        }
        }while (secondwronginput);

        System.out.println("Summa on: " + appendSum + " See lisati faili. ");




        try {
            FileWriter fileWriter = new FileWriter("outputsum.txt",true);
            //for (int i = 0; i < 10; i++) {
                fileWriter.append("Sisestatud arvude summa on: " +appendSum + System.lineSeparator());
                fileWriter.close();
            //}


        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ERROR");
        }
    }


}
