package it.vali;

public class HomeWork {

    //Meetod mis võtab parameetriks stringi massiivi ning tagastab lause,
    //kus iga stringi vahel on tühik

    static String makeStringArraytoString(String[] someStringArray) {
        //String madeToString = String.join(" ",someStringArray);
        //return madeToString;
        return String.join(" " ,someStringArray);
    }

    //Meetod mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja
    // Tagastada lause
    //vt eelmist ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldada.
    static String makeStringArraytoString(String[] someStringArray, String delimiter) {

        return String.join(delimiter ,someStringArray);
    }


    //Meetod, mis leiab nunbrite massiivist keskmise, ning tagastab selle
    static double returnsMidValueOfNrArray(int[]someNrArray){
        double sumOfNumberArray = 0;
        for (int i = 0; i < someNrArray.length ; i++) {
            sumOfNumberArray += someNrArray[i];
        }
        return sumOfNumberArray/someNrArray.length;
    }

    //Meetod, mis arvutab mitu % moodustab esimene arv teisest

    static float findPercentageOf (float howmanypercentIs,float from){

        return howmanypercentIs/from*100;
    }

    //Meetod mis leiab ringi ümbermõõdu raadiuse järgi
    static final double findCircumferenceByRadius (double radius){
        return radius*2*Math.PI;
    }


}
