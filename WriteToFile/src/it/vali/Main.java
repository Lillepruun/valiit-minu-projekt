package it.vali;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

            //Try blokis otsitakse /oodatakse exceptionit (erind, erand, või viga)
        try {
            //FileWriter on selline klass, mis tegeleb faili kirjutamisega
            // sellest klassis objekti loomisel antakse talle ette faili asukoht'
            //faili asukoht võib olla ainult faili nimega kirjutatud :output.txt
            //sel juhul kirjutatakse faili, mis asub samas kaustas kus meie Main.class

            //või täispika  asukohaga e:\\users//opilane/vali jne
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");

            for (int i = 0; i < 10; i++) {


            fileWriter.write(String.format("Elas metsas Mutionu %n"));
                fileWriter.write("Keset kuuski noori vamnu " + i+System.lineSeparator());
            //close on oluline, teised ei saa muidu faili kasutada, võtab mälu jne
        }

            fileWriter.close();


            //catch blok püütakse kinni kindlat tüüpi exception või kõik exceptionid, mis pärinevad antud exceptionist
        } catch (IOException e) {
            //PrintStackTrace tähendab, et prinditakse välja meetodite väljakutsumise hierarhia /ajalugu
            ///e.printStackTrace();
            System.out.println("ERROR: Antud failile ligiääs ei ole võimalik");
        }







    }



}
