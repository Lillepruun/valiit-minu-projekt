package it.vali;

// enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-na
enum Color {
    BLACK,
    WHITE,
    GREY,
}

enum ScreenType {
    LCD,
    TFT,
    OLED,
    AMOLED
}

public class Monitor {



    private String manufacturer;
    private double diagonal; //tollides "
    private Color color;
    private ScreenType screenType;
    private int year = 2000;

    public int getYear() {
        return year;
    }

    public double getDiagonal() {
        if (diagonal==0){
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }
    public void setDiagonal(double diagonal) {
        //this tähistab seda konkreetset objekti
        if (diagonal<0){
            System.out.println("Diagonaal ei saa olla negatiivne");
        } else if (diagonal>100){
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        }
        else {
            this.diagonal = diagonal;
        }
    }

    public String getManufacturer() {
        return manufacturer;
    }
    //Ära luba seadistada monitoritootjaks Huawei
    //Kui keegi soovib seda tootjat monitoritootjaks panna, pannakse hoopis tootjaks tekst: tootja puudub
    //Keela ka tühja tootjanime lisamine "", null
    public void setManufacturer(String manufacturer) {
        if (manufacturer==null){
            this.manufacturer = "Tootja puudub";
        }else
        if (manufacturer.equals("Huawei")
//        ||manufacturer.equals("")
//        ||manufacturer.equals(null)
        ){
            //System.out.println("Tootja puudub");
            this.manufacturer = "Tootja puudub";

        } else if (manufacturer.isEmpty()){
            this.manufacturer = "Tootja puudub";
        }else if (manufacturer.isBlank()){
            this.manufacturer = "Tootja puudub";
        } else {
            this.manufacturer = manufacturer;
        }

    }
    //Kui ekraani tüüp on seadistamata (null) siis tagasta tüübiks LCD

    public void setColor(Color color) {
        this.color = color;
    }
    public Color getColor(){
        return color;
    }

    public ScreenType getScreenType() {
        if (screenType==null){
            return ScreenType.LCD;
        }
        return screenType;
    }
    public void setScreenType(ScreenType screenType){
        this.screenType = screenType;
    }

//
//    public ScreenType getScreenType() {
//        return screenType;
//    }
//
//    public void setScreenType(ScreenType screenType) {
//        this.screenType = screenType;
//    }






     public void printInfo (){
         System.out.println();
         System.out.println("Monitori info:");
         System.out.printf("Tootja: %s %n",manufacturer);
         System.out.printf("Diagonaal: %.1f %n",diagonal);
         System.out.printf("Värv: %s %n",color);
         System.out.printf("Ekraani tüüp: %s %n",getScreenType());
         System.out.printf("Aasa: %d %n",year);
         System.out.println();




     }
     //tee meetod, mis tagastab ekraani diagonaalo cm-tes
//    static double convertDiagonalToCM(double diagonal){
//         double diagonalInCM = diagonal * 2.54;
//         return  diagonalInCM;
//    }
     public double diagonalToCm(){
        return  diagonal * 2.54;

    }



}
