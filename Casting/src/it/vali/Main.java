package it.vali;

public class Main {

    public static void main(String[] args) {
	// Casting on ühest arvutüübist teise
        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine (implicit casting)

        short b = a;
       // byte c = b; nt nii ei saa teha, sest short ei mahu byte sisse(peab ise kontrollima kas mahub
        //kui üks tüüp ei pruugi mahtuda teise sisse, siis peab ise veenduma, et see number mahub teise tüüpi
        //ja kui mahub siis peab seda ise teisendama.
        // explicit casting

        short c = 300;
        byte d = (byte) c;
        System.out.println(d);

        long e = 1000000000000L;
        int f = (int) e;
        System.out.println(f);

        long g = f;
        g = c;
        g = d;

        float h = 123.23434F;
        double i = h;
        System.out.println(i);

        //Antud juhul toimub ümardamine
        double j = 55.1111111111111111111;
        float k = (float) j;
        System.out.println(k);

        double l= 12E50; // E on siin 12*10 astmes 50
        float m = (float)l;
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;
        System.out.println(q);

      int r = 2;
      double s = 9;
      //int /int = int
        //int/double ja double/int= double
        //double + int = double
        System.out.println(r / s);
        System.out.println(r / (float)s);
        float t = 12.55555F;
        double u = 23.555555;
        System.out.println(t / u);



    }
}
