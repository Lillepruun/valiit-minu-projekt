package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
// convertimine - ühest tüübist teise
        //Küsi kasutjalt 2 numbrit ja prindi välja nende summa


        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta esimene täisarvuline number");
        int a = Integer.parseInt(scanner.nextLine());

        System.out.println("Sisesta teine täisarvuline number");
        int b = Integer.parseInt(scanner.nextLine());
        System.out.println("Nende summa on: "+ (a + b));
        System.out.printf("Sinu valitud numbrid olid %d ja %d. Nende summa on: %d%n",a,b,a+b);

        //Küsi 2 reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta esimene number");
        double c = Double.parseDouble(scanner.nextLine());

        System.out.println("Sisesta teine number");
       double d = Double.parseDouble(scanner.nextLine());
        System.out.printf("Sinu valitud numbrid olid %f ja %f. Nende jagatis on: %f%n",c,d,c/d);
        System.out.printf("Sinu valitud numbrid olid %.2f ja %f.2. Nende jagatis on: %.2f%n",c,d,c/d);
//n my printf, I need to use %f but I'm not sure how to truncate to 2 decimal places:
//
//Example: getting
//
//3.14159
//
//to print as:
//
//3.14 Use this:
//
//printf ("%.2f", 3.14159);

        //Kui  ma tahan näha, 0,56 asemel.....

        System.out.println(String.format(Locale.ENGLISH, "%.2f", c/d));

    }
}
