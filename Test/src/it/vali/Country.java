package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Country {

    private double population;
    private String name;
    List<String> languageArray = new ArrayList<>();

    public double getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLanguageArray() {
        return languageArray;
    }

    public void setLanguageArray(List<String> languageArray) {
        this.languageArray = languageArray;
    }


    @Override
    public String toString() {
        String info= "";
        for (int i = 0; i < languageArray.size(); i++) {
            info+= languageArray.get(i) + " ";
        }
        return String.format("Name: %s Population: %.0f Keeled: %s ",name,population,info);
    }
}
