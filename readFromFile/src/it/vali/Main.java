package it.vali;

import java.io.*;

public class Main {

    public static void main(String[] args) {
	// write your code here

       // String line = null;
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);


            //Readline loeb iga kord välja kutsudes järgmise rea
            //Kui readline avastab, et järgmist rida tegelikult ei ole, siis tagastab see meetod: null
//            while((line = bufferedReader.readLine()) != null) {
//                System.out.println(line);
//            }

            //teine võimalus, (ei ole eriti hea)
            String line =bufferedReader.readLine();
            do {
                line =bufferedReader.readLine();
                System.out.println(line);
            }
            while(line != null);



            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }




    }
}
