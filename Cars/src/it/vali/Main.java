package it.vali;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Car bmw =new Car();


        bmw.startEngine();
        bmw.startEngine();
        bmw.stopEngine();
        bmw.stopEngine();

        bmw.accelerate(100);
        bmw.startEngine();
        bmw.accelerate(100);

        Car fiat = new Car();
        Car mercedes = new Car();
        Car opel = new Car("opel","Vectra",1999,205);

        opel.startEngine();
        opel.accelerate(205);
        Car Tesla = new Car("Tesla","b3",2010,250,Fuel.ELECTRIC,true,false);
        Tesla.startEngine();
        Tesla.accelerate(150);

        Person jaanLillepruun = new Person("Jaan","Lillepruun",Gender.MALE,27);
        Person mari =new Person("mari","Orumäe",Gender.FEMALE,26);
        Person michaelKnight = new Person("Michael","Knight",Gender.MALE,30);
        Person personX = new Person("John","Doe",Gender.NOTSPECIFIED,40);
        Person Taavi =new Person("Taavi","Rõivas",Gender.MALE,40);

        Car kitt = new Car("Law and order foundation","Super",1970,500,Fuel.HYBRID,true, true);

        kitt.setDriver(michaelKnight);
        kitt.setOwner(personX);

        System.out.println("Kitt is driven by: " + kitt.getDriver().getFullNameOfPerson());
        System.out.println("Kitt is owned by: " + kitt.getOwner().getFullNameOfPerson());

        kitt.printInfo();

        kitt.startEngine();
        kitt.accelerate(300);

        System.out.println(kitt.getSpeed());

        kitt.printInfo();
        kitt.park();
        System.out.println(kitt.getSpeed());
        kitt.accelerate(200);
        kitt.startEngine();
        kitt.accelerate(250);
        System.out.println(kitt.getOwner().getAge());

//        List<Person> kittPassangers = new ArrayList<Person>();
//        kittPassangers.add(michaelKnight);
//        kittPassangers.add(personX);
//        kittPassangers.add(jaanLillepruun);
//        kittPassangers.add(mari);
//        kittPassangers.remove(0);

        kitt.setMaxPassangers(4);
        //kitt.setPassangers(kittPassangers);
        kitt.addPassangerObjToPassangerArraylist(michaelKnight);
        kitt.addPassangerObjToPassangerArraylist(jaanLillepruun);
        kitt.addPassangerObjToPassangerArraylist(mari);
        System.out.println("kitti current passangers: " +kitt.getCurrentNrOfPassangers());

        System.out.println(kitt.getPassangers().size());

        kitt.printNamesOfPassangers();
        kitt.addPassangerObjToPassangerArraylist(personX);
        kitt.printNamesOfPassangers();

        kitt.removePassangerObjFromPassangerArraylist(michaelKnight);
        kitt.addPassangerObjToPassangerArraylist(Taavi);
        kitt.printNamesOfPassangers();
        kitt.showPassengers();

        System.out.println(mari.toString());




    }
}
