package it.vali;


//Plane laiendab /pärineb klassist vehicle ja kasutab/implementeerib liidest Flyer

public class Plane extends Vehicle implements Flyer,Driver {
    private int maxDistance = 600;

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Fly plane");
    }
    @Override
    public int getMaxDistance(){
        return maxDistance;
    }
    @Override
    public void drive(){
        System.out.println("Sõidan lennukiga (maapinnal)");
    }
    @Override
    public void stopDriving (int afterDistance){
        System.out.println("Peatasin lennuki sõitmise (maapinnal)");
    }




    private void doCheckList(){
        System.out.println("Täidetakse checklist");
    }
    private void startEngine(){
        System.out.println("Mootor käivitus");
    }

}
