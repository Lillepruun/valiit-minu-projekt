package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class LivingplaceAbstract implements LivingPlace{

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    private List<Animal> animals = new ArrayList<>();

    public void setAnimalCounts(Map<String, Integer> animalCounts) {
        this.animalCounts = animalCounts;
    }

    //siin hoitakse infot, palju meil igat looma farmis on
    private Map<String,Integer> animalCounts = new HashMap<String,Integer>();

    public void setMaxAnimalCounts(Map<String, Integer> maxAnimalCounts) {
        this.maxAnimalCounts = maxAnimalCounts;
    }

    //siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String,Integer> maxAnimalCounts = new HashMap<String,Integer>();

//    public LivingplaceAbstract() {
//        maxAnimalCounts.put("Wolf",1);
//        maxAnimalCounts.put("Animal",3);
//        maxAnimalCounts.put("Animal",3);
//    }
    @Override
    public void addAnimal(Animal animal){
        //Kas animal on tüübist farmanimal või pärineb sellest tüübist
        if (!WildAnimal.class.isInstance(animal)){
            System.out.println("Selles elukohas ei saa see loom elada");
            return;
        }
        String animalType = animal.getClass().getSimpleName();



        if (!maxAnimalCounts.containsKey(animalType)){
            System.out.printf("Elukohas loomale %s kohta pole %n",animalType);
            return;
        }


        if (animalCounts.containsKey(animalType)){
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if (animalCount>=maxAnimalCount){
                System.out.printf("Elukohas on loomale %s kõik kohad juba täis %n",animalType);
                return;
            }

            animalCounts.put(animalType,animalCounts.get(animalType)+1);

            //sellist looma veel ei ole darmis
            //kindlasti sellele loomale kohta on
        }else {
            //siin oli viga?
            //animals.add((FarmAnimal) animal);
            animalCounts.put(animalType,1);
        }
        animals.add((Animal) animal);
        System.out.printf("ELukohta lisati loom %s%n",animalType);


    }

    //Tee meetod mis prindib välja kõik farmis elavad loomad ning mitu neid on
    //Pig 2
    //Cow 3
    //Tee meetod, mis eemaldab farmist looma
    @Override
    public void printAnimalCounts(){
        for (Map.Entry<String,Integer> entry : animalCounts.entrySet()) {
            System.out.printf("Looma %s on %d tükki %n",entry.getKey(),entry.getValue());
        }
        if (animalCounts.isEmpty()){
            System.out.println("Selles elukohas loomi pole");
        }

    }
    @Override
    public void removeAnimal(String animalType){
        //teen muutja, mis ma tahan, et iga element listist oleks : list mida tahan läbi käia
        //FarmAnimal animal hakkab olema järjest esimnene loom, siis teine loom, kolmas ja seni kuni loomi on

        //täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma
        //prindi "Farmis selline loom puiudub"
        boolean animalFound = false;
        for (Animal animal : animals) {
            if (animal.getClass().getSimpleName().equals(animalType)){
                animals.remove(animal);
                System.out.printf("Elukohast eemaldati loom %s %n",animalType);
                //Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                //muuljuhul vähenda animalCounts mapis seda kogust

                if (animalCounts.get(animalType)==1){
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }

                animalFound=true;
                break;
            }
        }
        if (!animalFound){
            System.out.println("Elukohas antud loom puudub");
        }
    }

}
