package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo extends LivingplaceAbstract{

    private List<Animal> animals = new ArrayList<>();
    private Map<String,Integer> animalCounts = new HashMap<String,Integer>();
    private Map<String,Integer> maxAnimalCounts = new HashMap<String,Integer>();

    public Zoo() {
        setMaxAnimalCounts(maxAnimalCounts);
        setAnimalCounts(animalCounts);
        setAnimals(animals);
        maxAnimalCounts.put("Rabbit",4);
        maxAnimalCounts.put("Zebra",4);
    }
}
