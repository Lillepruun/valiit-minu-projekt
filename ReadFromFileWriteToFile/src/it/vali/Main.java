package it.vali;

import java.io.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
	// write your code here

        //Loe failist input.txt iga teine rida  ning kirjuta need read faili output.txt


        try {

            //notepad kasutab vaikimisi ANSI encodingut. Selleks, et filereader oskaks seda korrektselt lugeda (täpitähti)m peame
            //talle ette ütlema, loe seda ANSI encodingus
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt",Charset.forName("Cp1252"));

            //Faili kirjutades on javas vaikeväärtus UTF-8. Antud juhul filewriteris me tegelikult ei peaks seda deklareerima, saab ilma ka
            //saame ka inputi kohe saveida UTF 8s, siis ei peaks midagi tegema

            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\outputfile.txt", Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);






            int loopCount = 0;
            String line = "";

               while(line != null) {



                line =bufferedReader.readLine();
                System.out.println(line);
                try {
                    if (loopCount %2 !=0){
                        fileWriter.write(line + System.lineSeparator());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Ei saa faili kirjtuda");
                }
                   loopCount++;
            }


            bufferedReader.close();
            fileReader.close();
            fileWriter.close();
    }


         catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }




    }
}
