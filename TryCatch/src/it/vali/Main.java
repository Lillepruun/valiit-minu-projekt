package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        int a = 0;
        try {
            int b = 4/a;

            String word = null;
            word.length();

        }

        //Kui on mitu viga, siis prindib ainult esimese vea ja edasi enam koodi ei loe
        //antud juhul int b-d ei hakata üldse lahendama
        //Kui on mitu catch blokki, siis otsib ta esimese bloki, mis oskab anutd exceptioni kinni püüda.
        //Kui tõsta Exceptin kõige ette, siis prindib antud kjuhul Exception bloki
        catch (ArithmeticException e){
            if (e.getMessage().equals("/ by zero")){
                System.out.println("Nulliga ei saa jagada");
            } else {
                System.out.println("Esines aritmeetiline viga");
            }

            //System.out.println(e.getMessage());
        } catch (RuntimeException e){
            System.out.println("Reaalajas esinev viga");
        }
        //Excpetion on klass, millest kõik  exceptionid tüübid pärinevad, mis omakorda tähendab, et
        //püüdes kinni selle üldise exceptioni, püüame kinni kõik Exceptionid
        catch (Exception e) {
           // e.printStackTrace();
            System.out.println("Esines viga. Ei saa toimingut lõpuni viia");
        }


        System.out.println();
        System.out.println();

        //Küsime kasutajalt numbri ja kui number ei ole korrektne, siis ütleme mingi veateate
        Scanner scanner = new Scanner(System.in);


        boolean wrongInput = false;
        do {
            wrongInput = false;
            System.out.println("Sisesta number");
            try {

                int number = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                System.out.println("Sisend on vales formaadis");
                wrongInput=true;

            }

        } while (wrongInput);

        //Loo töisarvude massiiv viie täisarvuga ning ürita sinna lisada kuues täisarv
        //näita veateadet

        int [] numbersArray = new int[5];

        try {
            for (int i = 0; i <numbersArray.length+1 ; i++) {
                numbersArray[i]=i+2;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("VIGA. Indeks, kuhu tahtsid väärtust määrata, ei eksisteeri");
            System.out.println();
        }
        divide(3,0);
        System.out.println(divide(3,0));
    }

    static double divide (int a, int b)throws ArithmeticException{
        return (double) a/b;
    }

}
