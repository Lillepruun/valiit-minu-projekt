package it.vali;

public class Main {

    public static void main(String[] args) {
        //Muutujat saab deklareerida ka kahe sammuga

	    String word;
	    word = "Kala";
        int number;
	    number = 3;
	    int secondNumber = -7;

	    int korrutis = number*secondNumber;

	    System.out.println(number);
        System.out.println(secondNumber);
		System.out.println(number + secondNumber);
//%n reavahetus või \n  või \r\n (trükimasina ajaloost)

		// Arvude 3 ja -7 korrutis  on -21
		//%n tekitab spetsiifilise reavhateuse (windows \r\n ja Linux/mac \n
		//System.lineSeparator() on ka võimalik
		System.out.printf("Arvude %d ja %d %n korrutis on %d%n",number,secondNumber,number*secondNumber);
		System.out.printf("Arvude %d %n ja %d %n korrutis on %d%n",number,secondNumber,korrutis);
int a=3;
int b=5;
		System.out.println("Arvude summa on " + a + b);
		//erinevad printimised!
		//String + number on alati string!
		//Stringi liitmisel numbriga teisendatakse number stringiks ja liidetakse kui liitsõna
		System.out.println(a + b);
		System.out.println("Arvude summa on " + (a + b));


		//kahe täisarvu jagamisel on tulemuseks jagatise täisosa ehk kõik peale koma süüakse ära
		double c = 2;
		double d = 8;
		double jagatis= c/d;
		System.out.println("Arvude "+ c + " " + " ja " + d + " jagatis on " + (c/d));

		//System.out.printf("Arvude %d %n ja %d  jagatis on  %d%n",c , d , jagatis); ??

		int maxInt = 2147483647;
		int lala= maxInt + 1;
		int lala1= maxInt + 2;
		System.out.println(lala);
		System.out.println(lala1);

		int minInt= -2147483648;
		int lala2 = minInt -1;
		System.out.println(lala2);

		short f = 32199;
		//long väärtust andes peab numbrile L tähe lõppu lisama
		long g = 99999999999999L ;
		long h= 234;
		System.out.println(f+g);
    }
}
