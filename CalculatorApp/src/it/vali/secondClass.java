package it.vali;

public class secondClass {

    public static int sumcalculator(int a, int b){
        int sum = a + b;
        return sum;
    }

    public static int subtract(int a, int b){
        int subtract = a - b;
        return subtract;
    }
    public static double divide(double a, double b){
        return a/b;
    }
    public static double multiply(double a, double b){
        return a*b;
    }

    //Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ning tagastab nende summa

    public static int addArrayTogether(int[] array){
        int sum =0;
        for (int i = 0; i <array.length ; i++) {
            sum+=array[i];
        }

        return sum;
    }

    //Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle.
    public static int[] turnArrayArround(int[] array){

        int[] newArray =new int[array.length];
        for (int i = 0, j =array.length-1; i <array.length ; i++,j--) {
            newArray[i] = array[j];

        }
        return  newArray;

    }


    //Meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult seal massiivis on numbrid stringidenda)
    //ja teisendab numbrite massiiviks ning tagastab selle.

    public static int[] makeStringArrayToIntArray(String[]stringArray){
        int [] madeToIntArray = new int[stringArray.length];
        for (int i = 0; i <stringArray.length ; i++) {
            madeToIntArray[i] = Integer.parseInt(stringArray[i]);
        }
        return madeToIntArray;
    }

    //meetod mis prindib välja massiivi elemendid
    public static void printNumbersArray(int[]numbersAsText) {
        for (int i = 0; i < numbersAsText.length; i++) {
            System.out.println(numbersAsText[i]);
        }
    }

}
