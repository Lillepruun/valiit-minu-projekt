package it.vali;

public class Pet extends DomesticAnimal {

    private int lifeExpectancy;

    public int getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(int lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }



    public void enterTheHouse (){
        System.out.println("Lemmikloom sisenes majja");
    }



    @Override
    public void printInfo(){

        super.printInfo();
        System.out.printf("Oodatav eluiga: %s%n",lifeExpectancy);
    }




}




