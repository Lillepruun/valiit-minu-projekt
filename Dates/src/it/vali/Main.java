package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here

        System.out.println(currentDateTimeToString());
        System.out.println();
        currentDateToString();





    }


    static String currentDateTimeToString(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    static void currentDateToString(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        //move calendar to yesterday
        //calendar.add(Calendar.DATE,-1);
        Date date = calendar.getTime();
        System.out.println(dateFormat.format(date));
        //lisame ühe aasta
        //calendar.add(Calendar.YEAR,1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        //Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad
        System.out.println(calendar.get(Calendar.MONTH));
        System.out.println();
        //selle aasta selle kuu esimene kuupäev
        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        System.out.println();

        int monthsLeftThisYear = 11-calendar.get(Calendar.MONTH);
            DateFormat weeldayFormat = new SimpleDateFormat("dd/MMMM/yyyy HH:mm:ss EEEE" );
        for (int i = 0; i < monthsLeftThisYear; i++) {
            calendar.add(Calendar.DATE,1);
            date = calendar.getTime();
            System.out.println(weeldayFormat.format(date));
        }
        System.out.println();



        //sama asi whilega
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH,1);

        while (calendar.get(Calendar.YEAR)==currentYear){
            date = calendar.getTime();
            System.out.println(weeldayFormat.format(date));
            calendar.add(Calendar.MONTH,1);

        }





    }










}
