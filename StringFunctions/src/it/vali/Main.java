package it.vali;

public class Main {

    public static void main(String[] args) {

        String sentence = "Elas metsas Mutionu keset kuuski noori vanu";

        //sümbolite indeksid tekstis algavad samamoodi indeksiga 0, nagu massiivides

        //Leia üles esimene tühik, mis on tema indeks?

        int spaceIndex = sentence.indexOf(" ");

        //võtab arvesse ka suuri ja väikeseid tähti nt, e vs E
        System.out.println(spaceIndex);


        System.out.println();
        //indexOf tagastab -1 kui otsitakse fraasi(tähte/sümbolit) mida ei leitud
        //ning indeksi(kust sõna algab) kui fraasi leitakse

       int secondSpaceIndex = sentence.indexOf(" ",spaceIndex + 1);



        System.out.println(secondSpaceIndex);
        System.out.println();

        //prindi välja kõik tühikute indeksid lauses
        //

        //!!!!!!!!!!!!!!!!!!!!!!
        spaceIndex = sentence.indexOf(" ");

        System.out.println();

        while (spaceIndex != -1) {

            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ",spaceIndex + 1);

        }

        System.out.println();

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
        spaceIndex = sentence.lastIndexOf(" ",spaceIndex -1);

        }


        //prindi välja lausest esimesed 4 tähte

        String part1 = sentence.substring(0,4);
        System.out.println(part1);

        String part = sentence.substring(5,11);
        System.out.println(part);

        sentence = "" +
                "jooksis kmetsas Mutionu keset kuuski noori vanu";


        spaceIndex = sentence.indexOf(" ");

        String firstWord = sentence.substring(0,spaceIndex);
        System.out.println("spaceIndex = " + spaceIndex);
        System.out.println("firstWord = " +  firstWord);

        secondSpaceIndex = sentence.indexOf(" ",spaceIndex + 1);
        String secondWord = sentence.substring(spaceIndex +1,secondSpaceIndex);
        System.out.println("secondWord = " + secondWord);

        System.out.println();
        System.out.println();
        System.out.println();

        sentence = "jooksis kmetsas Mutionu keset kuuski noori vanu";



        //Leia esimene k tähega algav sõna
        // et leiaks ka siis kui sõna on lause esimene sõna
        spaceIndex = sentence.indexOf("k");

//        int kcounter = 0;
//
//
//        while (spaceIndex!=-1) {

            if (spaceIndex == 0) {

                secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
                String kWord = sentence.substring(spaceIndex, secondSpaceIndex);

                System.out.println("kword is first word: " + kWord);
                //kcounter++;

            } else if (spaceIndex != 0) {
                spaceIndex = sentence.indexOf(" k");
                secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
                String kWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
                System.out.println("spaceIndex = " + spaceIndex);
                System.out.println("kWord = " + kWord);
                //kcounter++;
//            }
        }

       // System.out.println("kcounter = " + kcounter);




        System.out.println();
        System.out.println();
        System.out.println();


        //Lauri näide   VAATA VEEL SEDA LAURI GITIST. EI OLE PÄRIS SAMA
        System.out.println();
        System.out.println("Lauri näide:");
        System.out.println();

        String firstLetter = sentence.substring(0,1);
        String kWord = "";
        if(firstLetter.equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring(0,spaceIndex);
        }
        else {
            int kIndex =sentence.indexOf(" k") + 1;
        }

        int kIndex = sentence.indexOf(" k") + 1;
        spaceIndex = sentence.indexOf(" ", kIndex);
        String kword = sentence.substring(kIndex, spaceIndex);


        //Leia mitu sõna on lauses


        spaceIndex = sentence.indexOf(" ");
        int spaceCounter = 0;

        while (spaceIndex != -1) {


            spaceIndex = sentence.indexOf(" ",spaceIndex + 1);
            spaceCounter++;

        }
        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter+1);


        sentence = "jooksis kmetsas Mutionu keset kuuski noori vanu";

        //Leia mitu k tähega algavat sõna on lauses

        int kcounter = 0;
        sentence =sentence.toLowerCase();
         kIndex =sentence.indexOf(" k");
         while (kIndex!=-1) {
             kIndex = sentence.indexOf(" k",kIndex+1);
             kcounter++;
         }
         if (sentence.substring(0,1).equals("k")) {
             kcounter++;
         }
         System.out.printf("K tähega algavate sõnade arv lauses on %d %n",kcounter);






        //testin CharAt
        //char e võrreldakse == ga, kui tahad .equalsiga, siis pead objekti tegema

        char ch1 = 'a';
        char ch2 = 'a';

        if (ch1==ch2) {

            System.out.println("A==B ");

        }

        // ei pea panema siinkohal new Character ette

        Character char1Obj = ch1;
        Character char2Obj = new Character(ch2);

        if (char1Obj.equals(char2Obj)) {

            System.out.println("char1Obj.equals(char2Obj ");

        }










    }
}
