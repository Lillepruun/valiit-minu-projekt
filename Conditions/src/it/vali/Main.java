package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Sisesta number");
        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        //a = 2.23;
        // a = "Tere";
        //Java on Type safe language, seega nii javas teha ei saa


        if (a == 3) {
            System.out.printf("Arv %d on võrdne kolmega %n", a);
        }
        if (a<5) {
            System.out.printf("Arv %d on väiksem viiest %n", a);
        }
        if(a!= 4){
            System.out.printf("Arv %d ei ole neli %n", a);
        }
        if(a>= 2){
            System.out.printf("Arv %d on suurem kahest %n", a);
        }
        if(a>= 7){
            System.out.printf("Arv %d on suurem või võrdne seitsmega %n", a);
        }
        if(!(a>= 7)){
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega %n", a);
        }
        //arv a on 2 ja 8 vahel

        //Kui tingimuste vahel on &&, kui pks tingimustest on vale, siis edasi enam ei vaadata. Seega arvutusressursi koha pealt on mõtekas
        //panna esimeseks kõige tõenäolisema vale tingimuse.
        //Kui asjade vahel ||, ja esimene juba õige, siis edasi enam ei vaadata. Ka siin sama põhimõte.

        if((a> 2&&a<8)){
            System.out.printf("Arv %d on 2 ja 8 vahel %n", a);

        }
        //arv on väiksem kui 2 või suurem kui 8
        if((a< 2||a>8)) {
            System.out.printf("Arv %d on väiksem kui 2 või suurem kui 8 %n", a);
        }
        //kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
        if((a> 2&&a<8)||(a> 5&&a<8)||a>10) {
            //saab ka ilma sulgudeta kuna && on prioriteetne, soovitatakse ikkagi selguse mõttes panna sulud
            System.out.printf("Arv %d on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10 %n", a);
        }

        // Kui arv ei ole 4 ja 6 vahel aga on 5 ja 8 vahel
        //või arv on negatiivne aga pole suurem kui 14 (pole väiksem kui -14)
        if(!(a>4 && a<6) && (a>5 && a<8)||(a<0&&!(a<-14))) {
            System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel\n" +
                    " või arv on negatiivne aga pole väiksem kui -14 %n", a);
        }

        else {
            System.out.printf("Arv %d on suur %n", a);
        }
    }
}
