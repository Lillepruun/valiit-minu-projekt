package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Language {


    String languageName = "";

    private List<String> countryNames = new ArrayList<String>(){};



    @Override
    public String toString() {
        return String.join(", ",countryNames);
    }

    public List<String> getCountryNames(List<String> englishCountryNames) {
        return countryNames;
    }

    public void setCountryNames(List<String> countryNames) {
        this.countryNames = countryNames;
    }


    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }



}
