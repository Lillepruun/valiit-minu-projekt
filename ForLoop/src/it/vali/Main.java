package it.vali;

public class Main {

    public static void main(String[] args) {
        // For tsükkel on slline tsükkel, kus korduste arv on teada.

        //lõpmatu tsükkel
    /*    for (;;){
            System.out.println("Väljas on ilus ilm");
        }
*/
        //int i = 0, siin selles komponendis saab luua muutujaid
        //luuakse täisarv i mille väärtus hakkab tsükli sees muutuma
        // i<10 - tingimus mis peab olema tõene, et tsükkel käivituks ja korduks.
        //i++ - tegevus, mida iga tsükli korduse lõpus korratakse
        //i++ on sama mis i+1
        //i-- on sama mis i-1

        for (int i = 0; i < 5; i++) {
            System.out.println("Väljas on ilus ilm i= " + i);
            for (int j = 0; j < 10; j++) {
                System.out.println("shallla laa j= " + j);
            }
        }
        for (int i = 5; i > 1; i--) {
            System.out.println("Oki doki i= " + i);
            for (int j = 1; j < 10; j = j * 2) {
                System.out.println("tsiki briki j= " + j);


            }
        }

        System.out.println();
        //prindi ekraanile numbrid 1-10
        for (int i = 1; i < 11; i++) {
            System.out.println(i);
            //võime teha ka nii
        }

        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.println(i + 1);

        }

        System.out.println();
        for (int i = 0; i <= 10; i++) {
            System.out.println(i);

        }

        System.out.println();
        //prindi ekraanile numbrid 24st kuni 167

        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }//prindi ekraanile´numbrid 18st 3ni


        System.out.println();
        for (int i = 18; i >= 3; i--) {
            System.out.println(i);

        }
        System.out.println();
        //prindi ekraanile numbrid 2,4,6,8,10
        //i=i+2  => i+=2
        //i=i*3  => i*=3 sama jagatisega/
        for (int i = 2; i <= 10; i += 2) {
            System.out.println(i);

        }
        System.out.println();
        //prindi ekranaile numbrid 10 kuni 20 ja 40 kuni 60
        for (int i = 10; i <= 60; i++) {
            System.out.println(i);
            if (i==20) {
                i=40-1;
            }
        }
        //teine variant
        System.out.println("teine variant");
        for (int i = 10; i <= 60; i++) {
            if ( i<=20 || i>=40 ) {
                System.out.println(i);
            }
            }


        //PRidni kõik arvud. mis jaguvvaad 3ga vahemikus 10 kuni 50
        //a % 3
        // 4 % 3 => jääk 1
        // 6 % 3 => jääk 0
        //if (a % 3 == 0)
        for (int i = 10; i <= 50; i++) {

            if (i % 3 == 0) {
                System.out.println(i);
            }
            //et leida paaritud arvud if (i % 3 == 0)


        }

    }







    }
