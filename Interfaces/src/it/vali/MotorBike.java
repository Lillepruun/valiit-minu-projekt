package it.vali;


//Driverit pole vaja implementida kuna see pärineb juba DriverInTwoWheelsist
public class MotorBike extends Vehicle implements DriverInTwoWheels {

    @Override
    public int getMaxDistance(){
        return 100;
    }
    @Override
    public void drive(){
        System.out.println("Sõidan mootorrattaga");
    }
    @Override
    public void stopDriving (int afterDistance){
        System.out.println("Mootorratas peatatatakse " + afterDistance + " km pärast");
    }


    @Override
    public void driveInRearWheel() {
        System.out.println("sõidan tagumisel rattal");
    }
}
