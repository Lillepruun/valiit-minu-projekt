package it.vali;

public class Main {




    //meetodid on mingid koodi osad, mis frupeerivad mingit teatud kindlat funktsionaalsust
    //kui koodis on korduvaid koodi osasid, võiks mõelda, et
    //äkki peaks nende kohta tegema eraldi meetodi.


    public static void main(String[] args) {
	// write your code here


        printHello();
        System.out.println();
        printHello(5);

        System.out.println();

        printText("ndsgfj");

        System.out.println();

        printText("say hello",5);

        System.out.println();
        printText("say jejeje",5,true);
        System.out.println();
        Number number = 125426.34735;

        printText("say something",5,true);



    }


    //Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello (){
        String sayHello = "Hello";

            System.out.println(sayHello);

    }



    private static void printHello (int printnrTimes){
        String sayHello = "Hello";
        for (int i = 0; i < printnrTimes; i++) {
            System.out.println(sayHello + " " + i);
        }
    }

    //Lisame meetodi, mis prindib etteantud teksti välja
    //printText

    private static void printText (String text){

        System.out.println(text);
    }

    //Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi.
    private static void printText (String text, int howManyTimes){
        for (int i = 0; i <howManyTimes ; i++) {
            System.out.println(text);
        }


    }
    //Lisame meetodi, mis prindib etteantud teksti välja ette antud kordi
    //lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte

    private static void printText (String text, int howManyTimes, boolean convertToUppercase){

        for (int i = 0; i <howManyTimes ; i++) {
            if (convertToUppercase){
                System.out.println(text.toUpperCase());
            }
                else {
                System.out.println(text);
            }
        }
        //Method OVERLOADING - meil on mitu meetodit sama nimega, aga erinevate parameetrite kombinatsiooniga
        //Meetodi ülelaadimne


        //OVERRIDING on teine asi, see on OOP juures

        //Kui on samade parameetritega meetodid, siis nende järgi tehakse vahet, mis järjekorras neid sisestatakse.
        //sama järekorraga sama tüüpe parameetreid ei saa luua


    }












}
