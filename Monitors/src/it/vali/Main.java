package it.vali;
import static it.vali.Monitor.*;
public class Main {

    public static void main(String[] args) {
	// write your code here

        Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color =Color.WHITE;
        firstMonitor.diagonal=27;
        firstMonitor.screenType = ScreenType.AMOLED;

        secondMonitor.manufacturer = "Sony";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;


        System.out.println(firstMonitor.manufacturer);
        System.out.println(firstMonitor.color);
        System.out.println(secondMonitor.screenType);

        firstMonitor.color = Color.BLACK;
        System.out.println(firstMonitor.color);

        Monitor [] monitorsArray = new Monitor[3];

        // Lisa massivi 3 monitori
        //prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli
        Monitor thirdMonitor = new Monitor();
        thirdMonitor.manufacturer = "Huawei";
        thirdMonitor.color = Color.GREY;
        thirdMonitor.diagonal =26;
        thirdMonitor.screenType = ScreenType.OLED;

        monitorsArray [0] = firstMonitor;
        monitorsArray [1] = secondMonitor;
        monitorsArray [2] = thirdMonitor;

        System.out.println();

        for (int i = 0; i <monitorsArray.length ; i++) {
            if (monitorsArray[i].diagonal>25){
                System.out.println(monitorsArray[i].manufacturer);
            }
        }
        System.out.println();

        //leia suurima monitori värv
//        double biggestMonitordiagonal=0;
//        Color biggestMonitorColor=null;
        //lihtsam on teha uues objekt, ja neid võrrelda:
        Monitor maxSizeMonitor = monitorsArray[0];
        for (int i = 0; i <monitorsArray.length ; i++) {
            if (monitorsArray[i].diagonal>maxSizeMonitor.diagonal){
                maxSizeMonitor = monitorsArray[i];
                //biggestMonitordiagonal = monitorsArray[i].diagonal;
                //biggestMonitorColor=monitorsArray[i].color;

            }
        }

        System.out.println(maxSizeMonitor.manufacturer);
        System.out.println("biggest diagonal: " + maxSizeMonitor.diagonal);
        System.out.println(maxSizeMonitor.color);

        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();

        System.out.println(firstMonitor.diagonal);
        System.out.println(convertDiagonalToCM(firstMonitor.diagonal));

    }


}
