package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Methods {



    //Kontrollib kas sisestatud PIN on õige
     static boolean  isUserVerifiedByPIN (){
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {


        System.out.println("Sisesta PIN kood");

        if (scanner.nextLine().equals(loadPin())){
            System.out.println("PIN õige");
            return true;
            }
         }
        System.out.println("Vale PIN. Proovisite liiga palju kordi. Teie konto on suletud. \nPöörduge pangakontorisse");
        return false;
    }




    //toimub pin kirjutamine pin.txt faili
    private static  void saveNewPin() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Olete muutmas PIN koodi. Sisestage esmalt vana PIN kood");
        if (!isUserVerifiedByPIN()){
            System.exit(0);
        }

            String newPin;
            String newPinCheck;
            int loopcounter = 0;
            do {

            System.out.println("Sisesta uus pin");
            newPin = scanner.nextLine();
            System.out.println("Kinnita uus pin, sisestades see uuesti");
            newPinCheck = scanner.nextLine();
            loopcounter++;
            }while (!newPin.equals(newPinCheck)&&loopcounter!=3);

            //if pole vist vaja
            if (newPin.equals(newPinCheck)) {
                try {
                    FileWriter fileWriter = new FileWriter("pin.txt");
                    fileWriter.write(newPin + System.lineSeparator());
                    fileWriter.close();
                    System.out.println("Teie PIN on muudetud");
                } catch (IOException e) {
                    System.out.println("ERROR: Süsteemi viga! PIN koodi ei muudetud");
                    //e.printStackTrace();

                }
            }else {
                System.out.println("Teie pin koodi ei õnnestunud muuta. Proovige hiljem uuesti");
            }



    }


    //toimub pin välja lugemine pin.txt failist
    static String loadPin(){

        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Süsteemi viga");
        }
        return pin;
    }



    //toimub balance välja lugemine balance.txt
    static int loadBalance(){
        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance=Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Konto seisu ei leitud süsteemist");
        }
        return balance;
    }

    static void witchOperation (){
        System.out.println("Millist toimingut soovite teostada? Vajuta: ");
        System.out.println("a, kui soovid teha sularaha sissemaksu");
        System.out.println("b, kui soovid sularaha väljamakset");
        System.out.println("c, kui soovid näha kontojääki");
        System.out.println("d, kui tahad muuta PIN koodi");
        System.out.println("e, kui tahad konto väljavõtet");
        Scanner scanner = new Scanner(System.in);
        String operation = scanner.nextLine().toLowerCase();

        switch(operation) {
            case "a" :
                System.out.println("Sularaha sissemakse");

                saveTransactionRecord("Sularaha sissemakse");
                break;
            case "b" :
                System.out.println("Sularaha väljamakse");

                saveTransactionRecord("Sularaha väljavõte");
                break;
            case "c" :
                System.out.println("Kontojäägi vaatamine");
                showBalance();
                break;
            case "d" :
                System.out.println("PIN koodi muutmine");
                saveNewPin();
                break;
            case "e" : //Tehingute ajalugu
                showTransActionHistory();
                break;
            default :
                System.out.println("Sellist tehingut ei eksisteeri");
        }

    }

        //Meetod mis salvestav uue summa balance.txt faili
    static String cashInput(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisestage kupüürid   ...või kirjutage soovitud lisatav summa ekraanile ja vajutage Enter");

        int currentBalance = loadBalance();
        int inputAmount = Integer.parseInt(scanner.nextLine());
        String newBalance = String.valueOf( currentBalance+ Math.abs(inputAmount));

        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(newBalance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Raha ei õnnestunud kontole kanda");
        }
        System.out.println("Raha on kantud teie kontole");
        return String.valueOf(inputAmount);
    }

    static void showBalance(){
        System.out.println("Teie kontol on: " + loadBalance()+" EUR");
    }




    static String takeCash(){
        if (!isUserVerifiedByPIN()){
            System.exit(0);
        }
        System.out.println("Valige summa. Vajuta: ");
        System.out.println("a, 5 EUR");
        System.out.println("b, 20");
        System.out.println("c, 50");
        System.out.println("d, Muu summa");

        Scanner scanner = new Scanner(System.in);
        String operation = scanner.nextLine().toLowerCase();

        int withDrawal = 0;
        boolean wrongInput = false;
        switch (operation){
            case "a" : withDrawal = 5;
            break;
            case "b" : withDrawal = 20;
            break;
            case "c" : withDrawal = 50;
            break;
            case "d" :
                System.out.println("Sisesta väljavõetav summa");
                withDrawal = Integer.parseInt(scanner.nextLine());
                break;
            default :
                wrongInput =true;
                System.out.println("Sellist tehingut ei eksisteeri");
        }
        int currentBalance = loadBalance();
        String newBalance = String.valueOf(currentBalance - Math.abs(withDrawal));
        if (currentBalance-withDrawal>=0) {

            try {
                FileWriter fileWriter = new FileWriter("balance.txt");
                fileWriter.write(newBalance + System.lineSeparator());
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Raha ei õnnestunud kontolt võtta");
            }
            if (!wrongInput) {
                System.out.println("Võtke oma raha");
            }
        } else {
            System.out.println("Kontol ei ole piisavalt vahendeid");
        }
        return String.valueOf(withDrawal);
    }



    static String currentDateTimeToString(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    static void saveTransactionRecord(String transactionType){
        Methods  methods = new Methods();
        String minusOrPlus;
        String transaction;
        if (transactionType.equals("Sularaha väljavõte")){
            minusOrPlus =" -";
            transaction = takeCash();
        }else {
            minusOrPlus = " +";
            transaction =cashInput();

        }
        try {
            FileWriter fileWriter = new FileWriter("accountBalance.txt",true);
            fileWriter.write(currentDateTimeToString() +" "+ transactionType + minusOrPlus + transaction   + " Konto jääk: " + loadBalance() + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("tehingut ei õnnestunud logida");
        }
    }
    //konto väljavõte
    //sularaha sissemakse +100 eur
    //sual väljamakse - 90 jne
    static void showTransActionHistory (){

            String transactionHistory = "Tehingute ajalugu: ";
            try {
                FileReader fileReader = new FileReader("accountBalance.txt");
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while (transactionHistory!=null){
                    System.out.println(transactionHistory);
                    transactionHistory = bufferedReader.readLine();

                }

//
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("Konto seisu ei leitud süsteemist");
            }

        }
    static void showTransActionHistoryTEST (){

        String transactionHistoryLine = "";
        try {
            FileReader fileReader = new FileReader("accountBalance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            Date date=new Date();
            do {

                try {

                    date = dateFormat.parse(bufferedReader.readLine());
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }while (date!=null);
//            {
//            //System.out.println(transactionHistory);
//                String dateReadFromFile =transactionHistory;
//            //System.out.println(dateReadFromFile);
//
//            Date date=new Date();
//            try {
//
//             date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateReadFromFile);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
////
////            //date =  Date.from(dateReadFromFile);
//                if (transactionHistory != null) {
//                    System.out.println(transactionHistory + " "+date);
//                }
//                transactionHistory = bufferedReader.readLine();
//
//            }

//
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Konto seisu ei leitud süsteemist");
        }

    }







}
