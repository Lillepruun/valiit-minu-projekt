package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here


        //Ülesanne 1
        System.out.println("Ülesanne 1");
        double valueOfPi = Math.PI;
        double doublePi =valueOfPi*2;
        System.out.println("Pi väärtus: " + doublePi);
        //või
        System.out.println("Pi väärtus:" + valueOfPi*2);

        System.out.println();

        System.out.println("Ülesanne 2");
        //Ülesanne 2 väljakutsumine
        System.out.println(isEqual(10,10));
        System.out.println(isEqual(10,12));
        System.out.println();

        //Testing stringArrayLengthTointArray

        String[] stringArray = new String[]{"Tere", "maja","pikksõna"};
        int[] someIntArray = stringArrayLengthTointArray(stringArray);


        System.out.println("Ülesanne 3");
        for (int i = 0; i <stringArray.length ; i++) {
            System.out.println(someIntArray[i]);
        }


        //Ülesanne 4 Testing
        System.out.println();
        System.out.println("Ülesanne 4");

        System.out.println("sajand: " + returnCentury(1));
        System.out.println("sajand: " + returnCentury(128));
        System.out.println("sajand: " + returnCentury(598));
        System.out.println("sajand: " + returnCentury(1624));
        System.out.println("sajand: " + returnCentury(1827));
        System.out.println("sajand: " + returnCentury(1996));
        System.out.println("sajand: " + returnCentury(2017));
        System.out.println("sajand: " + returnCentury(2019));

        System.out.println();


        //Ülesanne 5
        System.out.println("Ülesanne 5");
        Country spain = new Country();
        List<String> spainLanguages = new ArrayList<>();
        spainLanguages.add("spanish");
        spainLanguages.add("english");
        spainLanguages.add("suahili");
        spainLanguages.add("portugese");
        spain.setName("Spain");
        spain.setPopulation(5000000);
        spain.setLanguageArray(spainLanguages);

        System.out.println(spain);
        System.out.println(spain.toString());






    }


    //Ülesanne 2
    public static boolean isEqual(int number, int secondNumber){
        if (number==secondNumber){
            return true;
        }
        else return false;
    }



    //Ülesanne 3
    public static int[] stringArrayLengthTointArray(String[] stringArray){
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i <stringArray.length ; i++) {
            intArray[i]=stringArray[i].length();
        }
        return intArray;
    }


    //Ülesanne 4
    public static byte returnCentury(int year){

        if (year<1||year>2018){
            return -1;
        }
        int century;
        if (year % 100 == 0) {
            century =  year / 100;
        } else {
            century = (year / 100) + 1;
        }
        return (byte)century;

    }

}
