package it.vali;

public class Main {

    public static void main(String[] args) {

        //järjend, massiiv, nimekiri
        //mõte on selles, et hoida sama liiki muutujaid
        //luuakse täisarvude massiiv, millesse mahub 5 elementi
        //loomise hetkel määratud elementide arvu hiljem muuta ei saa
        int[] numbers = new int[5];

        //massiivi indeksid algavad 0-ist!!, mitte 1st
        //viimane indeks on alati 1 võrra väiksem kui massiivi pikkus
        numbers [0] = 2;
        numbers [1] = 7;
        numbers [2] = -2;
        numbers [3] = 11;
        numbers [4] = 1;

        // 5 on juba out of bounds
        //numbers [5] = 2;
        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);


        System.out.println();

        for (int i = 0; i < 5; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();
//prindi vastupidises järjestuses
        for (int i = 4; i >= 0; i--) {
            System.out.println(numbers[i]);
        }

        System.out.println("prindi, mis on suuremad kui kaks");
        //prindi, mis on suuremad kui kaks
        for (int i = 4; i >= 0; i--) {
            if (numbers[i]>2) {
                System.out.println(numbers[i]);
            }
        }
        //prindi kõik paarisarvud
        System.out.println("prindi kõik paarisarvud");
        for (int i = 4; i >= 0; i--) {
            if (numbers[i]%2 ==0) {
                System.out.println(numbers[i]);
            }
        }
        //prindi tagant poolt kaks esimest paaritut arvu
        System.out.println("prindi tagant poolt kaks esimest paaritut arvu");
        int counter = 0;
        for (int i = 4; i >= 0; i--) {

            if (numbers[i]%2 !=0) {


                    System.out.println(numbers[i]);
                    counter++;
                    if (counter == 2) {
                        break;
                    }

            }
        }

        //Loo teine massiiv 3le numbrile ja pane sinna esimesest massiivist 3 esimest numbrit
        //Prindi teise massiivi elemendid ekraanile

        int[] teinenumbers = new int[3];


        for (int i = 0; i <3 ; i++) {
            teinenumbers [i]= numbers [i];
        }

        System.out.println();

        for (int i = 0; i < 3; i++) {
            System.out.println(teinenumbers[i]);
        }

        System.out.println();
        //Loo kolmas massiiv 3le numbrile ja pane sinna esimesest massiivist 3 numbrit tagant poolt alates

        int[] kolmasnumbers = new int[3];

        for (int i = 0; i <teinenumbers.length ; i++) {


        //lambi valem, allpool on parem
            kolmasnumbers [i]= numbers [numbers.length-i-1];
        }


        for (int i = 0; i < 3; i++) {
            System.out.println(kolmasnumbers[i]);
        }



        System.out.println();
        ///teine võimalus:

        for (int i = 0, j = numbers.length-1; i < kolmasnumbers.length; i++,j--) {

            kolmasnumbers [i]= numbers [j];
        }
        for (int i = 0; i < 3; i++) {
            System.out.println(kolmasnumbers[i]);
        }














    }
}
