﻿--see on lithne hello world teksti päring, mis tagastab ühe rea ja veeru
--veerul puudub pealkiri. Selles veerus ja reas saab olema tekst Hello World
SELECT 'Hello World';

--täisarvu saab märkida ilma ülakomadeta
SELECT 3;

SELECT 'raud' + 'tee'; --ei tööta PostgreSQL'is. Näiteks microsoft sql-is töötab

--CONCAT töötab kõigis erinevates sql serverites
SELECT CONCAT('raud','tee',4, 'all', 'maa','.');

--Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter','Paat', 23, 75.45, 'Blond'

--AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS eesnimi,
	'Paat' AS Perekonnanimi,
	23 AS vanus,
	75.45 AS kaal,
	'Blond'AS juuksevärv
	
	--tagastab praeguse puupäeva ja kellaaja mingis vaikimisi formaadis
	
SELECT NOW() AS kuupäev
--Kui tahan konkreetset osa sellest näiteks aastat või kuud
SELECT date_part('month',NOW()) AS kuupäev

--kuupäeva teisendamine/formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(),'hh24:mm:ss YYYY-MM-DD') AS kuupäev
--Sama asi, paneb komad vahele, või misiganes nende asemele panen
SELECT to_char(NOW(),'hh24:mm:ss YYYY,,,,MM,,,,DD') AS kuupäev

SELECT NOW()-interval '1 week ago'
SELECT NOW()-interval '1 year 2 months ago'
SELECT NOW()+ interval '2 centuries 23 years 4 months '
--INTERVAL laseb lisada või eemaldada mingit ajaühikut

--tabeli loomine
CREATE TABLE student (
	
	id serial PRIMARY KEY,  --serial tähendab, et tüübiks on int, mis hakkab 1 võrra suurenema
	--primary key(primaarvõti) tähendab, et see on unikaalne väli tabelis. 
	first_name varchar(64) NOT NULL,  --ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL,  --tohib tühi olla
	weight numeric(5,2) NULL,
	birthday date NULL
);

--tabelist kõikide ridade kõikide veergude küsimine
-- *tähendab : anna kõik veerud 
SELECT * FROM student

--Kui tahan mingit kuupäeva osa ise etteantud kuupäevast
SELECT date_part('month',TIMESTAMP '2019-01-01')

--Kui tahan mimnuteid näiteks kellaajast
SELECT date_part('minutes',TIME '10:10')

SELECT CONCAT(first_name, ' ', last_name) AS täisnimi FROM student;
SELECT CONCAT(first_name, ' ',date_part('year', birthday),' ', last_name) AS sünniaeg FROM student;


--Kui tahan filtreerida mingi tingimuse järgi
--WHERE statement
SELECT
	* 
FROM 
	student
WHERE height = 180



SELECT
	* 
FROM 
	student
WHERE first_name = 'Peeter' 
OR first_name ='Mari'



SELECT
	* 
FROM 
	student
WHERE first_name = 'Peeter' AND last_name = 'Puujalg'

--Küsi tabelist eesnime ja perekonnanime järgi mingi Peeter ja mingi Mari

SELECT
	* --või first_name kui tahan ainult eesnime
FROM 
	student
WHERE first_name = 'Peeter' AND last_name = 'Puujalg'
OR first_name = 'Mari' AND last_name = 'Maasikas'

--Vali õpilased, kelle pikkus jääb 170-185 vahele
SELECT
	*
FROM 
	student
WHERE 
height > 170 AND height < 185

--Anna õpilased, kes on pikemad kui 170 või lühemad kui 150
SELECT
	*
FROM 
	student
WHERE 
height > 170 OR height < 150

--Anna õpilase eesnimi ja pikkus kellel on sünnipäev jaanuaris

SELECT
	first_name AS eesnimi,
	height AS pikkus
FROM 
	student
WHERE 
 date_part('month',birthday) = 1
 
 
 --Anna mulle õpilased, kelle middle_name on null /või IS NOT NULL kui vastupidi
 SELECT
	*
FROM 
	student
WHERE 
 middle_name IS NULL
 
 --vali, kel´le pikkus ei ole 180
 SELECT
	*
FROM 
	student
WHERE 
 height <> 180 --või !=
 
 
 --Vali kelle pikkus on ...või..või
 SELECT
	*
FROM 
	student
WHERE 
 height IN (180,145,180)
 
 --Vali õpilsaed, kelle eesnimi on peeter, mari või kalle
 SELECT
	*
FROM 
	student
WHERE 
 first_name IN ('mari','Kalle','Peeter')
 
-- kelle nimi ei ole
 SELECT
	*
FROM 
	student
WHERE 
 first_name NOT IN ('Mari','Kalle','Peeter')
 
 
 --anna õpilased, kelle sünniaeg on kuu esimene, 4. või 7.  päev
SELECT
	*
FROM 
	student
WHERE 
date_part('day',birthday)  IN (1,4,7)


--Praegu peaks kõik valima, aga need mille height pole defineeritud (null), siis neid ei võta välja tegelikult
SELECT
	*
FROM 
	student
WHERE 
height > 0 OR height <=0
--nüüd võtab ka null
SELECT
	*
FROM 
	student
WHERE 
height > 0 OR height <=0 OR height IS NULL

--anna mulle õpilased pikkuse järjekorras lühemast pikemaks, kui pikkused võrdsed järjesta kaalu järgi
--kui pikkused võrdsed, järjesta.. kasutataks koma lihtsalt. DESC teeb vastupidi . Vaikimis on ASC
SELECT
	*
FROM 
	student
ORDER BY
height, weight

--Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused, mis jäävad 160 ja 170 vahele
SELECT
	*
FROM 
	student
WHERE height >160 AND height < 170
ORDER BY
birthday  


SELECT
	*
FROM 
	student
WHERE first_name LIKE 'P%'

--Kui tahan otsida sõna seest mingit sõnaühendit
SELECT
	*
FROM 
	student
WHERE first_name LIKE 'Ka%' -- EEsnimi algab Ka
WHERE first_name LIKE '%ee%' --sisaldab ee
WHERE first_name LIKE '%es' --lõppeb es

--tabelisse uute kirjete lisamine
INSERT INTO student
(first_name,last_name,height, weight, birthday, middle_name)
  VALUES
  ('Alar','Allikas',172,80.55,'1983-06-19', NULL),
  ('Tiiu','Tihane',165,67.55,'1987-04-19', 'Tiia'),
  ('Marje','Must',185,87.55,'1994-09-14', 'Marleen')

--TABELIS KIRJE MUUTMINE. UPDATE LAUSEGA PEAB OLEMA ETTEVAATLIK, ALATI PEAB KASUTAMA WHERE LAUSET
--muidu kirjutab terve tabeli üle
UPDATE
		student
SET
  height




UPDATE
		student
SET
  id  =14
  WHERE
  first_name = 'Alari' AND last_name = 'Kivisaar'
  
  --mitut rida korraga saab komaga lihtsalt
  UPDATE
		student
SET
  height = 178, weight = 79, birthday = '1978-02-23'
  WHERE
  first_name = 'Alari' AND last_name = 'Kivisaar'



--Muuda kõigi õpilaste pikkus ühe võrra suuremaks
UPDATE
		student
SET
  height = height + 1

--suurenda hiljem kui 1999 sündinud õpilsatel sünnipäeva ühepäeva võrra
UPDATE
		student
SET
  birthday= birthday +interval '1 day'
 WHERE date_part('years',birthday)>1999


--Kustutamisel olla ettevaatlik, alati kasuta WHERE-i
DELETE FROM
		student
		

 WHERE id = 4
 
 --CRUD operations   - Create. Read, Update, Delete
 --CReate (INSERT), Read (Select), Update, DElete



--Loo uus tabel loan, millel on väljad: amount(reaalarv), start_date, due_date, student_id  
CREATE TABLE loan (
	
	id serial PRIMARY KEY,  
	amount numeric(11,2) NOT NULL,  
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);


--lisa neljale õpilasele laenud
--kahele neist lisa veel üks laen
INSERT INTO loan 

	(amount, start_date, due_date, student_id)
	VALUES
	(24000, '2019-05-12','2028-10-13', 7),
	(14000, '2019-04-12','2027-03-17', 8)
	

--anna mulle kõik õpilased oma laenudega
SELECT 
* FROM student JOIN  loan ON student.id =loan.student_id



--vahetab kohad ära. et laenu saada lihtsalt, siis loan.*
SELECT 
 loan.*, student.*

 FROM student JOIN  loan ON student.id =loan.student_id
 
 
 
 
 
 
 
 SELECT 
student.first_name,
student.last_name,
loan.amount
 FROM student JOIN  loan ON student.id =loan.student_id



--anna mulle kõik õpilased oma laenudega
--aga ainult sellised laenud, mis on suuremad kui 500
--järjesta laenukoguse järgi
SELECT 
student.first_name,
student.last_name,
loan.amount
 FROM student JOIN  loan ON student.id =loan.student_id
WHERE loan.amount > 25000
ORDER BY  loan.amount



--anna mulle kõik õpilased oma laenudega
--aga ainult sellised laenud, mis on suuremad kui 500
--järjesta laenukoguse järgi
--INNER join on selline tabelite liitmine, kus liidetakse ainult need read, kus on võrdsed student.id = loan.student_id
--ehk need read, kus tablite vahel on seos, ülejäänud ignoreeritakse
SELECT 

student.first_name,
student.last_name,
loan.amount

 FROM student INNER JOIN  loan ON student.id =loan.student_id --INNER join on vaikimisi join, INNER SÕna võib ära jätta
WHERE loan.amount > 25000
ORDER BY  student.last_name DESC, loan.amount

--Loo uus tabel loan_type , milles väljad name ja description
CREATE TABLE loan_type (
	
	id serial PRIMARY KEY,  
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);


ALTER TABLE loan_type 
	ADD COLUMN loan_type_id int
	
	
--tabeli kustutamine
DROP TABLE tablename

--lisame mõned laenutüübid

INSERT INTO loan_type
(name, description)
VALUES
('Õppelaen', 'See on väga hea laen õpilastele'),
('SMS laen', 'See on väga halb laen '),
('Väikelaen', 'See on kõrge intressiga laen'),
('Kodulaen', 'See on väga hmadala intresssiga laen')



--Kolme tabeli inner join
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt 
	ON lt.id = l.loan_type_id


--INNER JOIN PUHUL tehete järejkord ei ole oluline
--sama asi
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN 
	
	loan AS l
	ON 	lt.id = l.loan_type_id
	
JOIN
	student AS s 
	ON s.id = l.student_id







--LEFT JOIN puhul võetakse joini esimesest (vasakust )tabelist kõik read 
--ning teises tabelis (Paremas) näidatakse puuduvatel kohtadel NULL

SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id

--LEFT JOIN puhul on järjekord väga oluline
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
	--annab kõik kombinatsioonid kahe tabeli vahel
	SELECT 
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN 
	student AS st
	WHERE
	s.first_name != st.first_name




--FULL OUTER JOIN on sama mis LEFT JOIN + RIGHT JOIN
SELECT 
	s.first_name,s.last_name
FROM
	student AS s
FULL OUTER JOIN 
	loan AS l
	ON s.id =l.student_id
	
	
	
	--anna kõigi kasutajate perekonnanimed, kes võtsid SMS laenu ja on võlgu üle 100 euro
	--Tulemused järjesta laenu võtja vanuse järgi väiksemast suuremaks
	SELECT 
	s.first_name,s.last_name
FROM
	student AS s
 JOIN 
	loan AS l
	ON s.id = l.student_id
JOIN 
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE  lt.name = 'SMS laen'
AND l.amount > 100
ORDER BY s.birthday



AGGREGATE functions
	
	
	--Keskmise leidnine, jäetakse välja read mille väärtus on NULL
	SELECT 
		AVG (height)
FROM
	student 
	
	
	
	!!!
	--Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses küsida mingit muud välja tabelist
	--sest agregaatfunktsiooni tulemus on alati ainult üks number ja seda ei saa kuidagi näidata koos väljadega, 
	--mida võib olla mitu rida
	
	--Palju on üks õpilane keskmiselt laenu võtnud
	--arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT 
		--student.first_name,  --seda ei saa panna praegu
		--student.last_name,
	AVG(COALESCE(loan.amount,0))

FROM
	student 

LEFT JOIN
loan
ON student.id = loan.student_id




--Palju on üks õpilane keskmiselt laenu võtnud
SELECT 
		--student.first_name,
		--student.last_name,
	ROUND(AVG(COALESCE(loan.amount,0))) AS "Keskmine laenusumma",
	MIN(loan.amount) AS "Minimaalne laenusumma",
	MAX(loan.amount)AS "Maksimaalne laenusumma",
	SUM(loan.amount) AS "Võlad kokku",
	COUNT (*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", --jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
	student 

LEFT JOIN
loan
ON student.id = loan.student_id



--KAsutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on GROUP BY-s ära toodud 
--(s.first_name, s.last_name)
--Teisi välju saab ainult kasutada agregaatfunktsioonide sees
SELECT 
		s.first_name, s.last_name, SUM(l.amount)
FROM
	student AS s
 JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name



--ANNA mulle laenude summad sünniaastate järgi
SELECT 
		date_part('year',s.birthday ), SUM(l.amount)
FROM
	student AS s
	
 JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY

	date_part('year',s.birthday )
--HAVING on nagu WHere, aga peale GROUP BY kasutamist
--Filtreerimisel saab kasuitada ainult neid välju, mis on GROUP BY-s ja agregaatfunktsioone
HAVING date_part('year',s.birthday ) IS NOT NULL
AND SUM(l.amount) > 1000



--Anna mulle laenude summad laenutüüpide järgi
SELECT 
		 lt.name, SUM(l.amount)
FROM
	loan AS l
	
LEFT JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY
	 lt.name 






--Tekita mingile õpilasele kaks sama tüüpi laenu
--anna mulle laenude summad grupeerituna õpilase ning laenu tüübi kohta
--nt mari õpplaen 2400 (1200 +1200)
--mari väikelaen 200 jne

SELECT 
		s.first_name, s.last_name,lt.name, SUM(l.amount)
FROM
	loan AS l
	
 JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
JOIN 
	student AS s
	ON l.student_id = s.id
GROUP BY
	s.first_name, s.last_name, lt.name
	





--ANNA mulle laenude summad sünniaastate järgi
SELECT 
		date_part('year',s.birthday ), SUM(l.amount)
FROM
	student AS s
 JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year',s.birthday )
--HAVING on nagu WHere, aga peale GROUP BY kasutamist
--Filtreerimisel saab kasuitada ainult neid välju, mis on GROUP BY-s ja agregaatfunktsioone
HAVING date_part('year',s.birthday ) IS NOT NULL
AND SUM(l.amount) > 1000  --Anna ainult need read, kus summa üle 1000
AND COUNT (l.amount) = 2   --Anna ainult need read, kus laene oli kokku 2

ORDER BY date_part('year',s.birthday )





--Anna mulle mitu laenu mingist tüübist on võetud  ning mis on nende summad
--sms laenu 2  -1000
--kodulaen  1  2300
--Anna mulle mitu laenu mingist tüübist on võetud  ning mis on nende summad
--sms laenu 2  -1000
--kodulaen  1  2300

SELECT 
		lt.name,COUNT(l.amount), SUM(l.amount) 
FROM
	loan AS l
	
 JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id

GROUP BY
	 lt.name


--Mis aastal sündinud võtsid kõige suurema summa laenu
--Mis aastal sündinud võtsid kõige suurema summa laenu

SELECT 
		date_part('year',s.birthday), SUM(l.amount)
FROM
	loan AS l
	
 JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id
	
	JOIN
	student AS s
	ON s.id = l.student_id

GROUP BY
	 date_part('year',s.birthday)

ORDER BY SUM(l.amount) DESC
	
	LIMIT 1 --ütleb, et anna ainult esimene rida
	
	
	
	
	
	--anna mulle õpilaste eesnime esitähe esinemise statistika
	--ehk siis mitme õpilase eesnimi algab migni tähega
	SELECT 
	SUBSTRING(first_name,1,1),
	COUNT (SUBSTRING(first_name,1,1))
FROM
	student 
	
GROUP BY
	 SUBSTRING(first_name,1,1)
ORDER BY
	 SUBSTRING(first_name,1,1)



--FORMAT
SELECT FORMAT('HEllO %s',233,233.22)

SELECT POSITION('B' in ('A asfhdszfjdzsgjB C'))
--WTF?
SELECT SUBSTRING('Lauri mattus',1,POSITION(' ' in 'Lauri mattus'))


--NESTED QUERIES!!!!!!!!!!!!
--Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT 
first_name, last_name
	FROM student
	WHERE
	height = (SELECT ROUND(AVG(height)) FROM student)
	
	
	
	
	--Anna mulle õpilased, kelle eesnimi on keskmise pikkusega õpilaste keskmine nimi

SELECT 
	first_name, last_name
	FROM student
	WHERE first_name IN

(SELECT 
 middle_name
	FROM student
	WHERE
	height = (SELECT ROUND(AVG(height)) FROM student ))
			
		
	
	
	--Lisa kaks vanimat õpilast töötajate tabelisse

INSERT INTO employee (first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday  LIMIT 2	


--Lisa kaks noorimat õpilast töötajate tabelisse

INSERT INTO employee (first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2	






--Anna mulle kõik matemaatikas õppivad õppilased
SELECT 
first_name, last_name
FROM 
student AS s
JOIN 
student_subject AS ss
ON s.id = ss.student_id
WHERE ss.subject_id = 1



--Anna mulle kõik õppeained, kus jaak õpib
SELECT 
name
FROM 
subject AS sub
JOIN 
student_subject AS ss
ON sub.id = ss.subject_id
WHERE ss.student_id = 7


--teisendamine täisaevuks
SELECT cast(round(AVG(age))AS UNSIGNED)  FROM test.emp99;


