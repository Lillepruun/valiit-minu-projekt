package it.vali;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Map<String, String> map = new HashMap<String, String>();

        //Sõnaraamat
        //key = value
        //Maja = house
        //Isa = dad jne

        //Map järjekorda ei salvestata, väljaprintimisel tuleb suvalises järkekorras
        //otsitakse alati key järgi
        //linkedHashMap-iiga saab järjekorda ka panna

        map.put("Maja","House");
        map.put("Isa","Dad");
        map.put("Puu","Tree");
        map.put("Sinine","Blue");


        // Oletame, et tahan teada, mis on inlgise keeles Puu
        String translation = map.get("Puu");
        System.out.println(translation);
        System.out.println();

        Map<String, String> idNumberName = new HashMap<String, String>();

        idNumberName.put("3268954978","Lauri");
        idNumberName.put("3474685475","Malle");
        idNumberName.put("3474685475","Kalle");

        //Kui kasutada sama key put(lisamisel), kirjutatakse value üle
        System.out.println(idNumberName.get("3474685475"));
        idNumberName.remove("3474685475");
        System.out.println(idNumberName.get("3474685475"));

        //Est = Estonia
        //Estonia +372

        //Loe lauses üle kõik erinevad tähed ning prindi välja  iga tähe järel mitu tükki teda selles lauses oli


        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();
        String sentence = "elas metsas mutionu";

        char symbolA = 'a';

        //CharArray?
        System.out.println();

        char[] someCharArray =sentence.toCharArray();
        for (int i = 0; i < someCharArray.length; i++) {
            if (letterCounts.containsKey(someCharArray[i])){
                letterCounts.put(someCharArray[i], letterCounts.get(someCharArray[i])+1);
            } else {
                letterCounts.put(someCharArray[i],1);
            }
        }
        //Map.Entry<Character,Integer>  on klass, mis hoiab endas ühte rida map-is
        //ehk ühte key-value kombinatsiooni/paari

        for (Map.Entry<Character,Integer> entry :letterCounts.entrySet()) {
            System.out.printf("Tähte '%s' esines %d korda %n",entry.getKey(),entry.getValue());
        }



        //e 2
        // l 1
        //a 2
        // s 3

        //checki lauri giti
        Map<String, String> linkedMap = new HashMap<String, String>();

        linkedMap.put("Maja","House");
        linkedMap.put("Isa","Dad");
        linkedMap.put("Puu","Tree");
        linkedMap.put("Sinine","Blue");

        for (Map.Entry<String,String> entry :linkedMap.entrySet()) {
            System.out.printf(" %s  %s  %n",entry.getKey(),entry.getValue());
        }
        SomeClass someClass = new SomeClass();
        System.out.println(someClass.hashCode());
        System.out.println("map = " + map);
        System.out.println(map);

        System.out.println("linkedMap = " + linkedMap);
        System.err.println(map);
        System.out.println("Main.main");
        System.out.println("args = [" + args + "]");
        System.out.println(map.keySet());
        map.notify();

    }
}
