package it.vali;

public class Main {

    public static void main(String[] args) {
	// write your code here



        LivingPlace livingPlace = new Farm();

        Pig pig = new Pig();
        pig.setName("Priit");
        livingPlace.addAnimal(pig);

        Pig secondpig = new Pig();
        secondpig.setName("Kalle");
        livingPlace.addAnimal(secondpig);

        Cow cow = new Cow();
        cow.setName("Malle");

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new FarmAnimal());
        livingPlace.addAnimal(new Horse());

//        farm.addAnimal(pig);
//        farm.addAnimal(pig);
//        for (int i = 0; i <16 ; i++) {
//            farm.addAnimal(new Sheep());
//        }

        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.addAnimal(secondpig);

        System.out.println();
        livingPlace.addAnimal(secondpig);
        livingPlace.removeAnimal("Kalle");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();
        livingPlace.removeAnimal("Pig");

        System.out.println();

        LivingPlace forest = new Forest();
        forest.printAnimalCounts();

        forest.addAnimal(new Wolf());
        forest.addAnimal(new Wolf());
        forest.addAnimal(new Wolf());
        forest.printAnimalCounts();
        forest.removeAnimal("Wolf");
        forest.printAnimalCounts();

        Zoo zoo = new Zoo();
        zoo.printAnimalCounts();
        zoo.addAnimal(new Zebra());
        zoo.printAnimalCounts();
        zoo.addAnimal(new Wolf());
        zoo.removeAnimal("Zebra");
        zoo.printAnimalCounts();



        //Mõelge ja täiendage Zoo ja Forest Klasse nii, et neil oleks nende 3 meetodi sisud



    }
}
