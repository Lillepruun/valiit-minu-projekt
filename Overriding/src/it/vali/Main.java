package it.vali;

public class Main {

    public static void main(String[] args) {
	// Method Overriding ehk ülekirjutamine

        //tähendab seda, et kuskil klassis millest antud klass pärineb, oleva meetodi sisu
        //päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle

        //superiga saan otsida vanemklassist
        //vaatab kogu ahela läbi, tagastab lähima



        Dog dog = new Dog();
        dog.eat();

        Cat angora = new Cat();
        angora.eat();
        angora.printInfo();

        //Kijruta koera getAge üle nii, et kui koera vanus on 0, siis näitaks ikka 1
        System.out.println(dog.getAge());
        System.out.println(dog.getName());
        CarnivoreAnimal bear = new CarnivoreAnimal();
        bear.printInfo();

        //metsloomadel printinfo võiks kirjutada Nimi: metsloomal pole nime


    }
}
